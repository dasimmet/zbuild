//! the executable for `zig build zb`
//!

const std = @import("std");
const ZBuild = @import("ZBuild");
const Proc = ZBuild.Proc;
const Zon = ZBuild.Zon;
const options = @import("options");
const ZonCli = @import("ZonCli.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();

    const args = try std.process.argsAlloc(arena.allocator());

    var com: Command = .{
        .args_full = args,
        .allocator = allocator,
        .stderr = std.io.getStdErr(),
        .stdout = std.io.getStdOut(),
        .stdin = std.io.getStdIn(),
    };
    if (args.len <= 1) {
        _ = try help(MainCli.usage)(&com);
        return;
    }

    if (MainCli.subcommands.get(args[1])) |subcom| {
        com.args = args[2..];
        const res = try subcom(&com);
        std.process.exit(res);
    }
    _ = try help(MainCli.usage)(&com);
    return error.UnknownCommand;
}

pub const SubCommandMap = struct {
    []const u8,
    SubCommand,
};
pub const SubCommand = *const fn (*Command) anyerror!u8;
pub const Command = struct {
    allocator: std.mem.Allocator,
    args: []const []const u8 = &[0][]const u8{},
    args_full: []const []const u8,
    stdout: std.fs.File,
    stderr: std.fs.File,
    stdin: std.fs.File,
};

pub fn help(comptime usage: []const u8) SubCommand {
    const wrapper = struct {
        pub fn help_cmd(com: *Command) !u8 {
            const argv = try std.mem.join(com.allocator, " ", com.args_full);
            defer com.allocator.free(argv);

            const stderr = com.stderr.writer();
            try stderr.print(usage, .{argv});
            return 0;
        }
    };
    return wrapper.help_cmd;
}

const MainCli = struct {
    pub const usage =
        \\cli: {s}
        \\usage: zig build zb -- <subcommand>
        \\
        \\# Available Subcommands
        \\
        \\## eval [zig build zb --] ...
        \\outputs a bash alias for "zbuild". If arguments are provided they will replace the zig command line in the alias definition,
        \\allowing to pass custom zig options
        \\
        \\## zon [--help|<subcommand>]
        \\build.zig.zon related commands
        \\
        \\## rawurl <relpath> [dir]
        \\same as `zon url` but pointing to a single file within the repository
        \\
        \\## compress <method> <mime_mode> {{<srcdir>| - }} [<output zigfile> | - ]
        \\writes all regular files in <srcdir> into a zig source code file
        \\using the <method> and `ZBuild.CompressMap`.
        \\the resulting file can be imported or compiled and run to retrieve
        \\its StaticStringMap entries
        \\
        \\## decompress <CompressMap file> [<entry path>] [<output file>| - ]
        \\runs `zig run <CompressMap file> -- [<entry path>] [<output file>| - ]`.
        \\if no <entry path> is provided, entries will be listed newline-separated.
        \\if no <output file> is provided or is "-", stdout is used.
        \\
    ;

    const subcommands = std.StaticStringMap(SubCommand).initComptime(
        .{
            .{ "-h", help(usage) },
            .{ "--help", help(usage) },
            .{ "eval", eval },
            .{ "zon", ZonCli.main },
            .{ "rawurl", rawurl },
            .{ "compress", compress },
            .{ "decompress", decompress },
            .{ "serve", serve },
        },
    );

    pub fn eval(com: *Command) !u8 {
        _ = try com.stdout.writeAll("alias zbuild=\"");
        if (com.args.len == 0) {
            _ = try com.stdout.writeAll("zig build zb --");
        } else {
            for (com.args, 0..) |a, i| {
                if (i > 0) _ = try com.stdout.writeAll(" ");
                _ = try com.stdout.writeAll(a);
            }
        }
        _ = try com.stdout.write("\"\nalias zbuild\n");
        return 0;
    }

    pub fn rawurl(com: *Command) !u8 {
        const stdout = com.stdout.writer();

        const relpath = if (com.args.len < 1)
            ""
        else
            std.mem.trim(u8, com.args[0], "/\\");

        const dir = if (com.args.len < 2)
            null
        else
            com.args[1];

        const hash = try Proc.git_rev(com.allocator, dir);
        const url = try Proc.git_url(com.allocator, dir);

        try Zon.Url.printRaw(stdout, .{
            .url = url,
            .ref = hash,
            .path = relpath,
        });
        _ = try stdout.write("\n");
        return 0;
    }

    pub fn compress(com: *Command) !u8 {
        const compress_usage = "\n\nzb compress <method> <mime_mode> {{<srcdir>| - }} [<output zigfile> | - ]\n\n";
        const cwd = std.fs.cwd();

        if (com.args.len < 3) {
            _ = try com.stderr.writeAll(compress_usage);
            return error.NotEnoughArguments;
        }
        const method = EnumArg(ZBuild.CompressMap.Method, com.args[0]) orelse .Unknown;
        if (method == .Unknown) return error.UnknownCompression;

        if (com.args.len <= 1) {
            _ = try com.stderr.writeAll(compress_usage);
            return error.NotEnoughArguments;
        }
        const mode = EnumArg(ZBuild.CompressMap.MimeMode, com.args[1]) orelse .None;

        const abspath = try cwd.realpathAlloc(com.allocator, com.args[2]);
        defer com.allocator.free(abspath);

        const fd = blk: {
            const output_p = if (com.args.len == 3) "-" else com.args[3];
            break :blk try createFileOrStdout(output_p);
        };
        defer fd.close();

        try ZBuild.CompressMap.fromDirectory(fd.writer().any(), .{
            .method = method,
            .allocator = com.allocator,
            .dir = abspath,
            .mime_mode = mode,
        });
        return 0;
    }

    pub fn decompress(com: *Command) !u8 {
        const args = com.args;
        switch (args.len) {
            0, 1 => {
                return Proc.call(.{
                    .allocator = com.allocator,
                    .argv = &[_][]const u8{
                        "zig",
                        "run",
                        args[0],
                    },
                });
            },
            2 => {
                return Proc.call(.{
                    .allocator = com.allocator,
                    .argv = &[_][]const u8{
                        "zig",
                        "run",
                        args[0],
                        "--",
                        args[1],
                    },
                });
            },
            3 => {
                return Proc.call(.{
                    .allocator = com.allocator,
                    .argv = &[_][]const u8{
                        "zig",
                        "run",
                        args[0],
                        "--",
                        args[1],
                        args[2],
                    },
                });
            },
            else => return error.TooManyArguments,
        }
    }

    pub fn serve(com: *Command) !u8 {
        if (@import("builtin").target.os.tag == .linux) {
            return serve_linux(com);
        }
        @panic("serve only implemented for linux");
    }
    pub fn serve_linux(com: *Command) !u8 {
        _ = try com.stdout.writeAll("alias zbuild=\"\n");
        const arg = try com.allocator.dupeZ(u8, com.args[0]);
        defer com.allocator.free(arg);
        _ = try com.stdout.writer().print("Watching: {s}\n", .{arg});

        const inotify_fd: i32 = @intCast(std.os.linux.inotify_init1(0));
        if (inotify_fd < 0) @panic("INOTIFY ERROR");
        const watch = std.os.linux.inotify_add_watch(inotify_fd, arg, std.os.linux.IN.ALL_EVENTS);
        _ = try com.stdout.writer().print("{any}\n", .{watch});
        var buf: [@sizeOf(std.os.linux.inotify_event) + std.fs.max_path_bytes]u8 = undefined;
        while (true) {
            const c = std.os.linux.read(inotify_fd, &buf, buf.len);
            const evt: *std.os.linux.inotify_event = @alignCast(@ptrCast(&buf));

            for (StructFlagEnum(std.os.linux.IN, u32).matches(evt.mask)) |n| {
                _ = try com.stdout.writer().print("{s}:{d},", .{ n.key, n.value });
            }
            _ = try com.stdout.write("\n");
            // const enumInfo = @typeInfo(std.os.linux.IN);
            // inline for (enumInfo.Struct.decls) |f| {
            //     const in_val: u32 = @intCast(@field(std.os.linux.IN, f.name));
            //     if ((evt.mask & in_val) != 0) {
            //         _ = try com.stdout.writer().print("evt: {s} {d}\n", .{ f.name, in_val });
            //     }
            // }
            if (evt.len != 0) {
                const path = evt.getName().?;
                const fullpath = try std.fs.path.join(
                    com.allocator,
                    &.{ com.args[0], path },
                );
                _ = try com.stdout.writer().print("{any} name: {s}\n{s}\n", .{ evt, path, fullpath });
            } else {
                _ = try com.stdout.writer().print("{any}\n", .{evt});
            }
            if (c <= 0) break;
        }
        return 0;
    }
};

fn StructFlagEnum(T: type, inttype: type) type {
    return struct {
        const flagTuple = struct {
            key: []const u8,
            value: inttype,
        };
        pub inline fn matches(it: inttype) []flagTuple {
            const structInfo = @typeInfo(T).@"struct";
            var buf: [structInfo.decls.len]flagTuple = undefined;
            var pos: usize = 0;
            inline for (structInfo.decls) |f| {
                if (@TypeOf(@field(T, f.name)) == inttype) {
                    const in_val: inttype = @intCast(@field(T, f.name));
                    if ((it & in_val) != 0) {
                        buf[pos] = .{
                            .key = f.name,
                            .value = in_val,
                        };
                        pos += 1;
                    }
                }
            }
            return buf[0..pos];
        }
    };
}

pub fn EnumArg(comptime T: type, arg: []const u8) ?T {
    inline for (std.meta.fields(T), 0..) |f, i| {
        if (std.ascii.eqlIgnoreCase(f.name, arg)) {
            return @enumFromInt(i);
        }
    }
    return null;
}

pub fn openFileOrStdin(path: []const u8) !std.fs.File {
    return if (std.mem.eql(u8, path, "-"))
        std.io.getStdIn()
    else
        (try std.fs.cwd().openFile(path, .{}));
}

pub fn createFileOrStdout(path: []const u8) !std.fs.File {
    return if (std.mem.eql(u8, path, "-"))
        std.io.getStdOut()
    else
        (try std.fs.cwd().createFile(path, .{}));
}
