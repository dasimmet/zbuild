pub const c = @cImport({
    @cInclude("zlib.h");
    @cInclude("GL/gl.h");
    @cInclude("sqlite3.h");
});
const std = @import("std");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    for (@typeInfo(c).@"struct".decls) |d| {
        try stdout.print("DECL: {s}\n", .{d.name});
    }
    try stdout.print("ZLIIB_VERSION: {s},{any}\n", .{ c.ZLIB_VERSION, c.z_stream_s });
}
