const std = @import("std");
const cli = @import("cli.zig");
const options = @import("options");
const Manifest = @import("Zig.Manifest");
const ZBuild = @import("ZBuild");
const Proc = ZBuild.Proc;
const Zon = ZBuild.Zon;

pub const usage =
    \\cli: {s}
    \\usage: zig build zb -- zon {{--help|<subcommand>}}
    \\
    \\# Available Subcommands
    \\
    \\## zon upd <dependency name> {{repo <path>|remote <git_url>|<url>}}
    \\updates the build.zig.zon file in your current working directory by setting the provided url
    \\or reading the url from a git repository's current commit at path
    \\
    \\## zon url [dir]
    \\return a url to the current commit for a build.zig.zon dependency.
    \\it runs `git show-ref` and `git config remote.origin.url`
    \\and maps the git remote url to their artifact download scheme
    \\
    \\## zon print
    \\prints dependencies from `build.zig.zon`
    \\
    \\## zon jzon
    \\converts json to zon
    \\
    \\## zon jzig
    \\converts json to zig
    \\
;

pub fn main(com: *cli.Command) !u8 {
    if (com.args.len > 0) {
        if (commands.get(com.args[0])) |subcom| {
            com.args = com.args[1..];
            return try subcom(com);
        }
    }
    return cli.help(usage)(com);
}
pub const commands = std.StaticStringMap(cli.SubCommand).initComptime(
    .{
        .{ "-h", cli.help(usage) },
        .{ "--help", cli.help(usage) },
        .{ "upd", upd },
        .{ "url", url },
        .{ "print", print },
        .{ "jzon", jzon },
        .{ "jzig", jzig },
    },
);

fn jzon(com: *cli.Command) !u8 {
    const out_path = if (com.args.len <= 1) "-" else com.args[1];
    const fd = try cli.createFileOrStdout(out_path);
    defer fd.close();

    const in_path = if (com.args.len < 1) "-" else com.args[0];
    const parsed = try readJson(com.allocator, in_path);
    defer parsed.deinit();

    try Zon.Json.writeZon(
        parsed.value,
        fd.writer(),
        4,
    );
    return 0;
}

fn jzig(com: *cli.Command) !u8 {
    const out_path = if (com.args.len <= 1) "-" else com.args[1];
    const fd = try cli.createFileOrStdout(out_path);
    defer fd.close();

    const in_path = if (com.args.len < 1) "-" else com.args[0];
    const parsed = try readJson(com.allocator, in_path);
    defer parsed.deinit();

    try Zon.Json.writeZig(
        parsed.value,
        fd.writer(),
        4,
    );
    return 0;
}

fn readJson(allocator: std.mem.Allocator, path: []const u8) !std.json.Parsed(std.json.Value) {
    const fd = try cli.openFileOrStdin(path);
    defer fd.close();

    const content = try fd.readToEndAlloc(
        allocator,
        std.math.maxInt(usize),
    );
    defer allocator.free(content);

    return std.json.parseFromSlice(
        std.json.Value,
        allocator,
        content,
        .{},
    );
}

fn upd(com: *cli.Command) !u8 {
    const allocator = com.allocator;

    if (com.args.len < 2) {
        _ = try com.stderr.write("\n\nzonupd <dep> {repo <path>|remote git_url|url}: Expected at least 2 arguments\n\n");
        return 1;
    }
    const dependency_name = com.args[0];
    const zurl =
        if (std.mem.eql(u8, com.args[1], "repo"))
        try Zon.Url.from_repo(allocator, com.args[2])
    else if (std.mem.eql(u8, com.args[1], "remote"))
        try Zon.Url.from_remote(
            allocator,
            com.args[2],
            if (com.args.len > 3) com.args[3] else "master",
        )
    else
        com.args[1];

    var out_buf = std.ArrayList(u8).init(allocator);
    defer out_buf.deinit();

    const hash = try Zon.Hash.fromUrlOpt(
        allocator,
        zurl,
        .{
            .cache_root = options.cache_root,
            .global_cache_root = options.global_cache_root,
        },
    );

    const dependency = .{
        .name = dependency_name,
        .url = zurl,
        .hash = hash,
    };

    const source = try std.fs.cwd().readFileAllocOptions(
        allocator,
        "build.zig.zon",
        std.math.maxInt(usize),
        null,
        8,
        0,
    );
    defer allocator.free(source);
    try out_buf.ensureTotalCapacity(source.len);
    var res = try Zon.patch(allocator, source, patch_manifest, dependency);
    defer res.deinit(allocator);

    res.patch_error catch |err| {
        switch (err) {
            error.DependencyNotFound => {
                std.log.err("\nDependency not found: {s}\n", .{dependency_name});
                return 1;
            },
            else => return err,
        }
    };
    _ = try std.fs.cwd().writeFile(.{
        .sub_path = res.source,
        .data = "",
    });
    return 0;
}

fn url(com: *cli.Command) !u8 {
    const file_path = if (com.args.len == 0)
        "."
    else
        com.args[0];

    const zurl = try Zon.Url.from_repo(com.allocator, file_path);
    _ = try com.stdout.write(zurl);

    _ = try com.stdout.write("\n");
    return 0;
}

fn print(com: *cli.Command) !u8 {
    const file_path = if (com.args.len == 0)
        "build.zig.zon"
    else
        com.args[0];

    const source = try std.fs.cwd().readFileAllocOptions(
        com.allocator,
        file_path,
        std.math.maxInt(usize),
        null,
        8,
        0,
    );
    var dep_map = std.StringHashMapUnmanaged([]const u8){};
    try Zon.apply(com.allocator, source, get_dep_urls, &dep_map);
    {
        var iter = dep_map.iterator();
        while (iter.next()) |it| {
            try std.fmt.format(com.stdout.writer(), "{s},{s}\n", .{ it.key_ptr.*, it.value_ptr.* });
        }
    }
    return 0;
}

fn patch_manifest(allocator: std.mem.Allocator, ast: *std.zig.Ast, dependency: anytype) !void {
    var token_replaced = false;
    var p = try Manifest.parse(
        allocator,
        ast.*,
        .{},
    );
    defer p.deinit(allocator);
    for (p.errors) |it| {
        std.log.err("{s}", .{it.msg});
    }
    if (p.errors.len > 0) return error.ParseManifestError;
    var iter = p.dependencies.iterator();

    while (iter.next()) |it| {
        if (std.mem.eql(u8, it.key_ptr.*, dependency.name)) {
            var v: Manifest.Dependency = it.value_ptr.*;
            if (v.location != .url) continue;
            if (token_replaced) {
                std.log.err(
                    "Error: dependency {s} is ambigous",
                    .{dependency.name},
                );
                return error.DependencyAmbigous;
            }

            if (!std.mem.eql(u8, dependency.url, v.location.url)) {
                std.debug.assert(v.hash.?.len == Manifest.multihash_hex_digest_len);

                try replace_string_literal(allocator, ast, it.value_ptr.*.location_tok, dependency.url);
                try replace_string_literal(allocator, ast, it.value_ptr.*.hash_tok, dependency.hash);

                v.location.url = dependency.url;
                v.hash = dependency.hash;
            }

            token_replaced = true;
        }
    }
    if (!token_replaced) return error.DependencyNotFound;
}

fn get_dep_urls(
    allocator: std.mem.Allocator,
    ast: *std.zig.Ast,
    ctx: anytype,
) anyerror!void {
    var p = try Manifest.parse(
        allocator,
        ast.*,
        .{},
    );
    for (p.errors) |it| {
        std.log.err("{s}", .{it.msg});
    }
    if (p.errors.len > 0) return error.ParseManifestError;
    var iter = p.dependencies.iterator();
    while (iter.next()) |dep| {
        const key = dep.key_ptr.*;
        const value = dep.value_ptr.*;
        try ctx.put(allocator, key, value.location.url);
    }
}

fn replace_string_literal(
    allocator: std.mem.Allocator,
    ast: *std.zig.Ast,
    tok: std.zig.Ast.TokenIndex,
    rep: []const u8,
) !void {
    const lok_tok = ast.tokens.get(tok);
    const next_tok = ast.tokens.get(tok + 1);
    const orig_source = ast.source;
    ast.source = try std.fmt.allocPrintZ(
        allocator,
        "{s}\"{}\"{s}",
        .{
            orig_source[0..lok_tok.start],
            std.zig.fmtEscapes(rep),
            orig_source[next_tok.start..],
        },
    );
    ast.tokens.set(tok, lok_tok);
    for (0..ast.tokens.len) |i| {
        const t = ast.tokens.items(.start)[i];
        if (t > lok_tok.start) {
            const old_t = t;
            _ = old_t;
            ast.tokens.items(.start)[i] =
                @as(u32, @intCast(ast.source.len)) +
                t -
                @as(u32, @intCast(orig_source.len));
        }
    }
}
