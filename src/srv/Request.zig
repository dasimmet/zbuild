const std = @import("std");
const zap = @import("zap");
const assets = @import("ZBuild.Srv.Assets");
const Response = @import("Response.zig");
const Api = @import("ZBuild.Srv.Api");

var lazyMap = assets.lazy(std.heap.c_allocator);
var lock = std.Thread.Mutex{};

pub fn on_request(comptime T: type, ctx: *T, r: zap.Request) void {
    if (r.path == null) {
        return Response.server_error(r, null, "500 - Header Error");
    }
    var path = std.mem.trimLeft(u8, r.path.?, "/");
    if (std.mem.eql(u8, path, "")) {
        path = "index.html";
    }

    lock.lock();
    defer lock.unlock();

    if (lazyMap.get(path) catch {
        lazyMap.reset();
        @panic("OOM");
    }) |res| {
        Response.render(r, path, res.*);
        lazyMap.free(path);
        return;
    }
    Api.on_request(ctx, r);
}
