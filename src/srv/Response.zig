const std = @import("std");
const zap = @import("zap");
const assets = @import("ZBuild.Srv.Assets");
const Sha256 = std.crypto.hash.sha2.Sha256;

const Response = @This();

pub fn render(r: zap.Request, path: []const u8, res: assets.EntryType) void {
    _ = path;
    if (res.sha256.len > 0) {
        r.setHeader("Etag", res.sha256) catch
            server_error(r, null, "500 - Set Etag Error");
        if (r.getHeader("If-None-Match")) |etag| {
            if (std.mem.eql(u8, etag, res.sha256)) {
                r.setHeader("Etag", res.sha256) catch
                    server_error(r, null, "500 - Set Etag Error");
                r.setStatus(.not_modified);
                return;
            }
        }
    }

    if (r.getHeader("If-Modified-Since")) |h| {
        const t = std.fmt.parseInt(u64, h, 10) catch {
            return server_error(r, null, "500 - Get Modtime Error");
        };
        if (t >= res.metadata.modified) {
            std.log.debug("cached: {d} {d}", .{ t, res.metadata.modified });
            r.setStatus(.not_modified);
            return;
        }
    }

    r.setStatus(.ok);
    // 31536000
    var content_encoding = switch (res.method) {
        .Deflate => "deflate",
        .Gzip => "gzip",
        .ZStd => "zstd",
        else => null,
    };

    if (r.getHeader("Accept-Encoding")) |enc| {
        if (content_encoding) |body_enc| {
            if (std.mem.indexOf(u8, enc, body_enc)) |c| {
                _ = c;
                content_encoding = null;
            } else {
                r.setHeader("Content-Encoding", body_enc) catch |err|
                    return send_error(r, "Content-Encoding", err, @errorReturnTrace());
            }
        }
    } else {
        if (res.method == .Deflate) {
            r.setHeader("Content-Encoding", content_encoding.?) catch |err|
                return send_error(r, "Content-Encoding", err, @errorReturnTrace());
        } else {
            content_encoding = null;
        }
    }

    r.setHeader("Content-Type", res.mimetype) catch
        return server_error(r, null, "500 - Set Content-Type Error");

    var buf: [assets.biggest_file]u8 = undefined;

    const modified = std.fmt.bufPrint(&buf, "{d}", .{res.metadata.modified}) catch |err|
        return send_error(r, "buffer overflow", err, @errorReturnTrace());
    r.setHeader("Last-Modified", modified) catch |err|
        return send_error(r, "Set Last-Modified", err, @errorReturnTrace());

    var writer = std.io.fixedBufferStream(&buf);

    if (content_encoding == null and res.method != .Raw) {
        _ = res.decompressBody(writer.writer(), 1024) catch |err|
            return send_error(r, "DecompressBody", err, @errorReturnTrace());

        r.sendBody(buf[0..@intCast(res.metadata.size)]) catch |err|
            return send_error(r, "sendBody", err, @errorReturnTrace());
    } else {
        r.sendBody(res.body) catch |err|
            return send_error(r, "500 - Sending Body Error", err, @errorReturnTrace());
    }
}

pub fn send_error(r: zap.Request, comptime msg: []const u8, err: anyerror, trace: ?*std.builtin.StackTrace) void {
    if (trace) |t| {
        std.log.err("Request Error: {},{s},{}", .{
            err,
            msg,
            t,
        });
    } else {
        std.log.err("Request Error: {},{s}", .{
            err,
            msg,
        });
    }
    if (@import("builtin").mode == .Debug)
        return r.sendError(err, if (@errorReturnTrace()) |current_trace| current_trace.* else null, 500);

    return server_error(r, err, msg);
}

pub fn server_error(r: zap.Request, err: ?anyerror, comptime msg: []const u8) void {
    const tpl = "<html><body><pre>Error: {?} {s}</pre></body></html>";
    var error_buf: [512 + tpl.len]u8 = undefined;
    const body = std.fmt.bufPrint(
        &error_buf,
        tpl,
        .{
            err,
            msg,
        },
    ) catch
        @panic("error buffer overflow");
    r.setStatus(.internal_server_error);

    r.sendBody(body) catch return;
}
