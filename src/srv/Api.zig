//!This is the default `ZBuild.Srv.Api` module for
//!passing into the `ZBuild.Step.Srv`
//!all decls here are optional and only called
//!if found at comptime

const std = @import("std");
const zap = @import("zap");

/// an optional []const u8 to a directory to serve
/// can also be set as an option to the `ZBuild.Step.Srv`
// pub const public_folder = "zig-out";

/// an anytype context to handle server side state
/// if omitted or not pub, functions need to accept '*void'
pub const Context = void;

/// this will run before serving requests
pub fn init(listener: zap.HttpListener) Context {
    _ = listener;
    std.debug.print("\nZBuild.Srv.Api on_init!\n", .{});
    return Context{};
}

pub fn on_request(ctx: *Context, r: zap.Request) void {
    _ = ctx;
    r.setStatus(.not_found);
    r.sendBody("<html><body><h1>404 - Not Found</h1></body></html>") catch return;
}

pub fn on_upgrade(ctx: *Context, r: zap.Request, target_protocol: []const u8) void {
    _ = ctx;
    if (!std.mem.eql(u8, target_protocol, "websocket")) {
        std.log.warn("received illegal protocol: {s}", .{target_protocol});
        r.setStatus(.bad_request);
        r.sendBody("400 - BAD REQUEST") catch unreachable;
        return;
    }
    std.log.info("connection upgrade OK", .{});
}
