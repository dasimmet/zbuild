//! The main Entrypoint for the ZBuild zap http server
//! It Requires an "assets" module containing the static assets to serve,
//!

const std = @import("std");
const builtin = @import("builtin");
const zap = @import("zap");
const Api = @import("ZBuild.Srv.Api");
const Options = @import("ZBuild.Srv.Options");
const Request = @import("Request.zig");

const Context = if (@hasDecl(Api, "Context")) Api.Context else void;
var global_context: Context = undefined;

pub const PublicFolder = union(enum) {
    None,
    /// a compile time constant path
    Comptime: []const u8,
    /// a precompiled prefix joined with a TODO: runtime argument
    RuntimePrefix: []const u8,
};

var public_folder: PublicFolder = if (@hasDecl(
    Options,
    "public_folder",
)) .{ .Comptime = Options.public_folder } else if (@hasDecl(
    Api,
    "public_folder",
)) .{ .Comptime = Api.public_folder } else .None;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{
        .thread_safe = true,
    }){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const argv = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, argv);

    for (argv) |a| {
        std.log.info("argv: {s}", .{a});
    }

    var listener = zap.HttpListener.init(.{
        .port = Options.port,
        .on_request = pass_req_context,
        .max_clients = 100000,
        // // other optional handlers:
        .on_upgrade = on_upgrade,
        .public_folder = switch (public_folder) {
            .Comptime => |p| p,
            else => null,
        },
        .log = true,
    });

    if (builtin.mode == .Debug or builtin.mode == .ReleaseSafe) {
        std.log.warn("Enabling Zap Debug log!", .{});
        zap.enableDebugLog();
    }

    if (@hasDecl(Api, "init")) {
        global_context = Api.init(listener);
    }

    try listener.listen();

    std.debug.print("\nOpen http://{s}:{d} in your browser\n", .{ "127.0.0.1", Options.port });

    // start worker threads
    zap.start(.{
        .threads = 4,
        .workers = 2,
    });
}

pub fn pass_req_context(r: zap.Request) void {
    if (@hasDecl(Api, "on_request")) {
        Request.on_request(Context, &global_context, r);
    }
}

pub fn on_upgrade(r: zap.Request, proto: []const u8) void {
    if (@hasDecl(Api, "on_upgrade")) {
        Api.on_upgrade(&global_context, r, proto);
    }
}
