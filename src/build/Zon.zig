const std = @import("std");
pub const Url = @import("ZonUrl.zig");
pub const Hash = @import("ZonHash.zig");
pub const Json = @import("JsonToZon.zig");

pub const PatchResult = struct {
    patch_error: anyerror!void,
    ast: *std.zig.Ast,
    source: []const u8,
    pub fn deinit(self: *PatchResult, allocator: std.mem.Allocator) void {
        self.ast.deinit(allocator);
        allocator.free(self.source);
    }
};

pub fn apply(
    allocator: std.mem.Allocator,
    source: [:0]const u8,
    applyFn: *const fn (std.mem.Allocator, *std.zig.Ast, ctx: anytype) anyerror!void,
    ctx: anytype,
) !void {
    var ast = try std.zig.Ast.parse(
        allocator,
        source,
        .zon,
    );
    defer ast.deinit(allocator);

    return applyFn(allocator, &ast, ctx);
}

pub fn patch(
    allocator: std.mem.Allocator,
    source: [:0]const u8,
    patchFn: *const fn (std.mem.Allocator, *std.zig.Ast, ctx: anytype) anyerror!void,
    ctx: anytype,
) !PatchResult {
    var out_buf = std.ArrayList(u8).init(allocator);
    defer out_buf.deinit();
    try out_buf.ensureTotalCapacity(source.len);

    var ast = try std.zig.Ast.parse(
        allocator,
        source,
        .zon,
    );

    const patch_error = patchFn(allocator, &ast, ctx);

    try ast.renderToArrayList(&out_buf, .{});
    return .{
        .patch_error = patch_error,
        .ast = &ast,
        .source = try out_buf.toOwnedSlice(),
    };
}
