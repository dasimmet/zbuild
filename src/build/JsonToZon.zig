//! helpers writing a `.zig` file to a writer from a `std.json.Value`
//! into a std.io.Writer

const std = @import("std");

/// iteration starter with depth 0 and to write header and footer
pub fn writeZig(Jvalue: std.json.Value, writer: anytype, indent: u8) !void {
    try writer.writeAll("pub const data=");
    try convert(Jvalue, writer, indent, 0);
    try writer.writeAll(";\n");
}

/// zon is the same as zig without header and footer starting at depth 0
pub fn writeZon(Jvalue: std.json.Value, writer: anytype, indent: u8) !void {
    try convert(Jvalue, writer, indent, 0);
    if (indent != 0) try writer.writeAll("\n");
}

// actual recursive writing function for json values
pub fn convert(
    Jvalue: std.json.Value,
    writer: anytype,
    indent: u8,
    depth: u8,
) !void {
    switch (Jvalue) {
        .null => {
            _ = try writer.write("null");
        },
        .bool => |v| {
            _ = try writer.print("{any}", .{v});
        },
        .integer => |v| {
            _ = try writer.print("{d}", .{v});
        },
        .float => |v| {
            try writer.print("{d}", .{v});
        },
        .number_string => |v| {
            try writer.print("{s}", .{v});
        },
        .string => |v| {
            try writer.print("\"{}\"", .{std.zig.fmtEscapes(v)});
        },
        .array => |v| {
            if (indent != 0) {
                _ = try writer.write(".{\n");
            } else {
                _ = try writer.write(".{");
            }
            for (v.items) |entry| {
                try write_indent(writer, (depth + 1) * indent);
                try convert(
                    entry,
                    writer,
                    indent,
                    depth + 1,
                );
                if (indent != 0) {
                    _ = try writer.write(",\n");
                } else {
                    _ = try writer.write(",");
                }
            }
            try write_indent(writer, depth * indent);
            _ = try writer.write("}");
        },
        .object => |v| {
            if (indent != 0) {
                _ = try writer.write(".{\n");
            } else {
                _ = try writer.write(".{");
            }
            var iter = v.iterator();
            while (iter.next()) |entry| {
                try write_indent(writer, (depth + 1) * indent);
                _ = try writer.print(".{}{s}", .{
                    std.zig.fmtId(entry.key_ptr.*),
                    if (indent != 0)
                        " = "
                    else
                        "=",
                });

                try convert(
                    entry.value_ptr.*,
                    writer,
                    indent,
                    depth + 1,
                );
                if (indent != 0) {
                    _ = try writer.write(",\n");
                } else {
                    _ = try writer.write(",");
                }
            }
            try write_indent(writer, indent * depth);
            _ = try writer.write("}");
        },
    }
}

inline fn write_indent(writer: anytype, indent: u16) !void {
    for (0..indent) |_| {
        _ = try writer.write(" ");
    }
}
