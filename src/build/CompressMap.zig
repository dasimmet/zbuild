//!Writes a number of files into a StaticStringMap

pub const CompressHeader_literal = @embedFile("CompressHeader.zig");
pub const CompressHeader = @import("CompressHeader.zig");
pub const Method = CompressHeader.Method;
const RecursiveDirIterator = @import("RecursiveDirIterator.zig");
const std = @import("std");
const builtin = @import("builtin");
pub const MimeExt = @import("gen/mime_extensions.json.zig");
pub const MimeMagic = @import("gen/mime_magic_numbers.json.zig");
pub const Empty = @import("CompressMapEmpty.zig");

//the size of buffer to read at the start of each file
pub const MIME_MAGIC_SIZE = 1024;

/// Options for `fromDirectory`
pub const DirOptions = struct {
    allocator: std.mem.Allocator,
    dir: []const u8,
    method: Method = Method.Deflate,
    progress: bool = true,
    mime_mode: MimeMode = .Any,
};

/// write a CompressMap to `writer` with entries from `DirOptions.dir`
pub fn fromDirectory(writer: std.io.AnyWriter, opt: DirOptions) !void {
    const prog: ?std.Progress.Node = if (opt.progress) std.Progress.start(.{
        .root_name = "CompressMap",
    }) else null;

    var ctx = Context{
        .writer = writer,
        .entry = .{
            .allocator = opt.allocator,
            .source = "",
            .dest = "",
            .default_method = opt.method,
            .method = opt.method,
            .mime_mode = opt.mime_mode,
        },
        .prog = prog,
    };

    try RecursiveDirIterator.run(
        opt.dir,
        incrementContext,
        &ctx,
    );

    try header(
        writer,
        opt.method,
        "application/octet-stream",
        ctx.biggest_file,
        ctx.count,
    );

    try addDir(
        opt.dir,
        &ctx,
    );

    try footer(writer);

    if (prog) |p| p.end();
}

/// Context for passing state through `RecursiveDirIterator`
pub const Context = struct {
    writer: ?std.io.AnyWriter = null,
    count: usize = 0,
    biggest_file: usize = 0,
    entry: EntryOptions,
    prog: ?std.Progress.Node,
};

pub fn addDir(path: []const u8, ctx: *Context) !void {
    try RecursiveDirIterator.run(
        path,
        addSubFile,
        ctx,
    );
}

/// add a file to the writer in Context
pub fn addFile(
    source_path: []const u8,
    entry_path: []const u8,
    ctx: *Context,
) !void {
    ctx.entry.source = source_path;
    ctx.entry.dest = entry_path;
    // if (ctx.prog) |p| {
    //     p.setUnit(entry_path);
    // }

    try entry(ctx.writer.?, &ctx.entry);
    if (ctx.prog) |p| {
        p.completeOne();
    }
}

/// add a file to the writer in Context with relative path to base
pub fn addSubFile(
    base: []const u8,
    entry_path: []const u8,
    ctx: *Context,
) !void {
    ctx.entry.source = entry_path;

    const unit = entry_path[base.len..];
    ctx.entry.dest = unit[std.fs.path.sep_str.len..];
    // if (ctx.prog) |p| {
    //     p.setUnit(unit);
    // }

    try entry(ctx.writer.?, &ctx.entry);
    if (ctx.prog) |p| {
        p.completeOne();
    }
}

/// increments the entry count and updates biggest_file
pub fn incrementContext(
    base: []const u8,
    entry_path: []const u8,
    ctx: *Context,
) !void {
    _ = base;
    ctx.count += 1;
    if (ctx.prog) |p| {
        p.setEstimatedTotalItems(ctx.count);
    }

    const fd = try std.fs.openFileAbsolute(
        entry_path,
        .{},
    );
    defer fd.close();
    const meta = try fd.metadata();
    ctx.biggest_file = @max(ctx.biggest_file, meta.size());
}

/// Writes the `CompressHeader` literally as well as `HeaderTemplate` filled with meta info
pub fn header(writer: anytype, default_method: Method, mimetype: []const u8, biggest_file: usize, count: usize) !void {
    _ = try writer.write(CompressHeader_literal);
    try std.fmt.format(writer, HeaderTemplate, .{
        std.zig.fmtEscapes(@import("builtin").zig_version_string),
        @tagName(@import("builtin").os.tag),
        biggest_file,
        std.zig.fmtEscapes(mimetype),
        std.zig.fmtId(@tagName(default_method)),
        count,
    });
    _ = try writer.write(HeaderTemplateDeps);
}

pub fn footer(writer: anytype) !void {
    try writer.writeAll(Footer);
}

pub const HeaderTemplate =
    \\
    \\/// HeaderTemplate
    \\
    \\pub const zig_version = "{}";
    \\pub const os_tag: std.Target.Os.Tag = .{s};
    \\pub const biggest_file = {d};
    \\pub const default_mimetype = "{}";
    \\pub const default_method: Method = .{};
    \\pub const map_size = {d};
    \\
;
pub const HeaderTemplateDeps =
    \\
    \\/// Dependant zig code on templated code above
    \\
    \\pub const EntryType = Entry(default_method, biggest_file);
    \\pub const EntryMapType = EntryMap(EntryType);
    \\const EvalBranchQuota = map_size * EvalBranchQuotaMultiplier;
    \\pub inline fn map() map_t {
    \\    return std.StaticStringMap(EntryType).initComptime(map_internal);
    \\}
    \\pub fn lazy(alloc: std.mem.Allocator) LazyMap(EntryType, map_t) {
    \\  return .{
    \\      .map = map(),
    \\      .buf = std.hash_map.StringHashMap(EntryType).init(alloc),
    \\  };
    \\}
    \\const map_t = blk: {
    \\    @setEvalBranchQuota(EvalBranchQuota);
    \\    break :blk @TypeOf(std.StaticStringMap(EntryType).initComptime(map_internal));
    \\};
    \\
    \\pub fn main() !void {
    \\    const cwd = std.fs.cwd();
    \\    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    \\    defer arena.deinit();
    \\
    \\    const args = try std.process.argsAlloc(arena.allocator());
    \\    if (args.len == 1) {
    \\        for (0..map_size) |i| {
    \\            const key = map().kvs.keys[i];
    \\            const stdout = std.io.getStdOut();
    \\            try std.fmt.format(stdout.writer(), "{s}\n", .{key});
    \\        }
    \\    } else {
    \\        const entry = map().get(args[1]).?;
    \\        const fd = if (args.len == 2 or std.mem.eql(u8, args[2], "-"))
    \\            std.io.getStdOut()
    \\        else
    \\            try cwd.createFile(args[2], .{});
    \\        defer fd.close();
    \\        _ = try entry.decompressBody(fd.writer(), 1024);
    \\    }
    \\}
    \\
    \\const map_internal = [_]EntryMapType{
;
pub const Footer = "};\n";

pub const MimeMode = enum {
    Magic,
    Extension,
    Any,
    None,
};

const EntryOptions = struct {
    allocator: std.mem.Allocator,
    source: []const u8,
    dest: []const u8,
    mime_mode: MimeMode = .Any,
    embed_full_path: bool = false,
    default_method: Method,
    method: Method,
    max_file_size: usize = std.math.maxInt(usize),
    biggest_file: usize = 0,
    indent: u8 = 2,
};

fn entry(writer: anytype, opt: *EntryOptions) !void {
    const fd = try std.fs.openFileAbsolute(
        opt.source,
        .{},
    );
    defer fd.close();

    try std.fmt.format(
        writer,
        ".{{\"{}\",.{{",
        .{
            std.zig.fmtEscapes(opt.dest),
        },
    );
    try indent(writer, opt.indent);

    {
        const meta = try fd.metadata();
        opt.biggest_file = @max(opt.biggest_file, meta.size());
        try writer.print(".metadata={},", .{
            formatFSZonMetadata(meta),
        });
        try indent(writer, opt.indent);
    }

    if (opt.method != opt.default_method) {
        try writer.print(
            ".method=.{s},",
            .{
                @tagName(opt.method),
            },
        );
        try indent(writer, opt.indent);
    }

    if (opt.embed_full_path) {
        try writer.print(
            ".full_path=\"{}\",",
            .{
                std.zig.fmtEscapes(opt.source),
            },
        );
        try indent(writer, opt.indent);
    }
    const mimetype: ?[]const u8 = switch (opt.mime_mode) {
        .Extension => read_mime_path(opt.source),
        .Magic => blk: {
            const magic = try read_mime_magic(fd, MIME_MAGIC_SIZE);
            try fd.seekTo(0);
            break :blk magic;
        },
        .Any => blk: {
            if (read_mime_path(opt.source)) |e|
                break :blk e;

            std.log.debug("source:{s}\n", .{opt.source});
            const magic = try read_mime_magic(fd, MIME_MAGIC_SIZE);
            try fd.seekTo(0);
            break :blk magic;
        },
        .None => null,
    };
    if (mimetype) |t| {
        try std.fmt.format(
            writer,
            ".mimetype=\"{}\",",
            .{
                std.zig.fmtEscapes(t),
            },
        );
        try indent(writer, opt.indent);
    } else {
        if (opt.mime_mode != .None)
            std.log.warn("Unknown Mimetype: {s}", .{opt.source});
    }
    {
        var body: []const u8 = undefined;
        defer opt.allocator.free(body);
        switch (opt.method) {
            .Unknown => return error.UnknownCompression,
            .Raw => {
                body = try fd.readToEndAlloc(
                    opt.allocator,
                    opt.max_file_size,
                );

                try std.fmt.format(
                    writer,
                    ".body=\"{}\",",
                    .{
                        std.zig.fmtEscapes(body),
                    },
                );
            },
            .Deflate => {
                body = try fd.readToEndAlloc(opt.allocator, opt.max_file_size);

                var compressed = std.ArrayList(u8).init(opt.allocator);
                defer compressed.deinit();

                var Compressor = try std.compress.flate.deflate.compressor(
                    .raw,
                    compressed.writer(),
                    .{
                        .level = .best,
                    },
                );
                // defer Compressor.deinit();

                _ = try Compressor.write(body);
                try Compressor.flush();
                try std.fmt.format(
                    writer,
                    ".body=\"{}\",",
                    .{
                        std.zig.fmtEscapes(compressed.items),
                    },
                );
            },
            .Gzip => {
                body = try fd.readToEndAlloc(opt.allocator, opt.max_file_size);
                var compressed = std.ArrayList(u8).init(opt.allocator);
                defer compressed.deinit();

                var Compressor = try std.compress.gzip.compressor(
                    compressed.writer(),
                    .{},
                );
                // defer Compressor.deinit();

                _ = try Compressor.write(body);
                try Compressor.flush();
                try std.fmt.format(
                    writer,
                    ".body=\"{}\",",
                    .{
                        std.zig.fmtEscapes(compressed.items),
                    },
                );
            },
            .XZ, .ZStd => {
                body = try file_to_mem(
                    opt.allocator,
                    opt.source,
                    opt.method,
                    opt.max_file_size,
                );

                try std.fmt.format(
                    writer,
                    ".body=\"{}\",",
                    .{
                        std.zig.fmtEscapes(body),
                    },
                );
            },
        }
        try indent(writer, opt.indent);
        {
            var md5_buf: [std.crypto.hash.Md5.digest_length]u8 = undefined;
            {
                var md5 = std.crypto.hash.Md5.init(.{});
                md5.update(body);
                md5.final(&md5_buf);
            }
            try std.fmt.format(
                writer,
                ".md5=\"{}\",",
                .{
                    std.fmt.fmtSliceHexLower(&md5_buf),
                },
            );
        }
        try indent(writer, opt.indent);
        {
            var sha256_buf: [std.crypto.hash.sha2.Sha256.digest_length]u8 = undefined;
            {
                var sha256 = std.crypto.hash.sha2.Sha256.init(.{});
                sha256.update(body);
                sha256.final(&sha256_buf);
            }
            try std.fmt.format(
                writer,
                ".sha256=\"{}\",",
                .{
                    std.fmt.fmtSliceHexLower(&sha256_buf),
                },
            );
        }
    }
    if (opt.indent > 0)
        try writer.writeAll("\n");

    try writer.writeAll("}},\n");
}

fn formatFSZonMetadata(meta: std.fs.File.Metadata) std.fmt.Formatter(formatZonMetadata) {
    return .{ .data = meta };
}

fn formatZonMetadata(
    meta: std.fs.File.Metadata,
    comptime fmt: []const u8,
    options: std.fmt.FormatOptions,
    writer: anytype,
) !void {
    _ = options;
    std.debug.assert(fmt.len == 0);
    const tpl =
        \\.{{
        \\    .size={d},
        \\    .accessed={d},
        \\    .modified={d},
        \\
    ;
    try writer.print(tpl, .{
        meta.size(),
        meta.accessed(),
        meta.modified(),
    });
    if (meta.created()) |c| {
        try writer.print("    .created={d},\n", .{c});
    }
    _ = try writer.write("  }");
}

/// Command line workaround for unsupported compressors
fn file_to_mem(
    allocator: std.mem.Allocator,
    path: []const u8,
    comp: Method,
    max_file_size: usize,
) ![]const u8 {
    var body: []const u8 = undefined;
    switch (comp) {
        .XZ => {
            const proc = try std.process.Child.run(.{
                .allocator = allocator,
                .argv = &[_][]const u8{ "xz", "-T0", "-9", "-c", "--", path },
                .max_output_bytes = max_file_size,
            });
            defer allocator.free(proc.stderr);

            body = proc.stdout;
        },
        .Gzip => {
            const proc = try std.process.Child.run(.{
                .allocator = allocator,
                .argv = &[_][]const u8{ "gzip", "-9", "-c", "--", path },
                .max_output_bytes = max_file_size,
            });
            defer allocator.free(proc.stderr);

            body = proc.stdout;
        },
        .ZStd => {
            const proc = try std.process.Child.run(.{
                .allocator = allocator,
                .argv = &[_][]const u8{ "zstd", "-T0", "-9", "-c", "--", path },
                .max_output_bytes = max_file_size,
            });
            defer allocator.free(proc.stderr);

            body = proc.stdout;
        },
        else => return error.NotImplemented,
    }
    return body;
}

/// write newline and indentations
fn indent(writer: anytype, num_indent: u8) !void {
    if (num_indent > 0) {
        _ = try writer.write("\n");
        for (0..@as(usize, num_indent)) |_| {
            _ = try writer.write(" ");
        }
    }
}

fn bufPtr(comptime bufsize: usize) type {
    return std.meta.Int(
        .unsigned,
        std.math.log2_int_ceil(usize, bufsize) / std.math.log2_int(usize, 2),
    );
}

fn read_mime_path(path: []const u8) ?[]const u8 {
    const extension = std.fs.path.extension(path);
    // need to set branch quota relative to tuple length
    @setEvalBranchQuota(MimeExt.data.len * 3);
    inline for (MimeExt.data) |it| {
        if (@hasField(@TypeOf(it), "deprecated") and it.deprecated == true) continue;
        inline for (it.fileTypes) |t| {
            if (std.ascii.eqlIgnoreCase(extension, t)) {
                return it.name;
            }
        }
    }
    return null;
}

//reads the first 'bufsize' bytes from a file and tries to match the mime magic rules
fn read_mime_magic(fd: std.fs.File, comptime bufsize: usize) !?[]const u8 {
    @setEvalBranchQuota(std.meta.fields(@TypeOf(MimeMagic.data)).len * 3);

    var longest_magic_len: usize = 0;
    var mimetype_found: ?[]const u8 = null;
    var content_buf: [bufsize]u8 = undefined;
    const bytes_read: usize = try fd.readAll(content_buf[0..]);
    const content = content_buf[0..bytes_read];

    inline for (MimeMagic.data) |filetype| {
        if (mime_match_weight(filetype, content)) |found_t| {
            if (found_t.weight > longest_magic_len) {
                mimetype_found = found_t.mime;
                longest_magic_len = found_t.weight;
            }
        }
    }
    return mimetype_found;
}

const MatchMimeType = struct {
    mime: []const u8,
    weight: usize,
};

//calculates the amount of bytes matching a mimetype definition
fn mime_match_weight(filetype: anytype, content: []const u8) ?MatchMimeType {
    var current_match: MatchMimeType = .{
        .mime = "",
        .weight = 0,
    };

    inline for (filetype.rules) |rule| {
        current_match.mime = comptime if (@hasField(@TypeOf(rule), "mime"))
            rule.mime
        else if (@hasField(@TypeOf(filetype), "mime"))
            filetype.mime
        else
            continue;
        if (comptime std.mem.eql(u8, rule.type, "or")) {
            if (mime_match_weight(rule, content)) |match| {
                current_match.mime = match.mime;
                current_match.weight += match.weight;
            }
        } else if (comptime std.mem.eql(u8, rule.type, "and")) {
            if (mime_match_weight(rule, content)) |match| {
                current_match.mime = match.mime;
                current_match.weight += match.weight;
            }
        } else if (comptime std.mem.eql(u8, rule.type, "equal")) {
            var hex_buf: [64]u8 = undefined;
            const magic = std.fmt.hexToBytes(&hex_buf, rule.bytes) catch return null;
            const rule_len = rule.end - rule.start;
            if (rule_len == magic.len and rule.end <= content.len) {
                const mime_magic_equals = std.mem.eql(u8, magic, content[rule.start .. rule.start + magic.len]);
                if (mime_magic_equals) {
                    // std.log.debug("match-mime: {s}, {s}\n", .{ mime, rule.bytes });
                    current_match.weight += magic.len;
                }
            }
        }
    }
    if (current_match.weight > 0) return current_match;
    return null;
}

test "compress" {
    var tmpdir = std.testing.tmpDir(.{});
    defer tmpdir.cleanup();

    const fd = try tmpdir.dir.createFile("assets.zig", .{});
    defer fd.close();
    const dir = try std.fs.cwd().realpathAlloc(std.testing.allocator, "src/build");
    defer std.testing.allocator.free(dir);
    const tdir = try tmpdir.dir.realpathAlloc(std.testing.allocator, ".");
    defer std.testing.allocator.free(tdir);
    std.debug.print("compress: {s}\ntmp: {s}\n", .{ dir, tdir });

    try fromDirectory(fd.writer().any(), .{
        .allocator = std.testing.allocator,
        .dir = dir,
        .progress = false,
    });
}
