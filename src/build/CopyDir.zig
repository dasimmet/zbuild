//!
//!

const std = @import("std");
pub const RecursiveDirIterator = @import("RecursiveDirIterator.zig");

pub const CopyDir = @This();
const Self = CopyDir;
allocator: std.mem.Allocator,
from: []const u8,
to: []const u8,
verbose: bool = false,

pub fn run(allocator: std.mem.Allocator, from: []const u8, to: []const u8) !void {
    var self: Self = .{
        .allocator = allocator,
        .from = from,
        .to = to,
    };
    return self.iterate();
}

pub fn iterate(self: *CopyDir) !void {
    try RecursiveDirIterator.run(
        self.from,
        copyFile,
        self,
    );
}

fn copyFile(
    base: []const u8,
    entry_path: []const u8,
    self: *CopyDir,
) !void {
    const cwd = std.fs.cwd();
    const basename = std.fs.path.basename(entry_path);
    const rel = entry_path[base.len + std.fs.path.sep_str.len ..];

    const dir = if (rel.len == basename.len)
        ""
    else
        rel[0 .. rel.len - std.fs.path.sep_str.len - basename.len];

    {
        const target_dir = try std.fs.path.join(
            self.allocator,
            &[_][]const u8{ self.to, dir },
        );
        defer self.allocator.free(target_dir);
        cwd.makePath(target_dir) catch |err| {
            if (err != error.PathAlreadyExists) return err;
        };
    }
    {
        const target = try std.fs.path.join(
            self.allocator,
            &[_][]const u8{ self.to, rel },
        );
        defer self.allocator.free(target);
        if (self.verbose) std.log.info("entry: {s},{s}", .{ self.to, rel });
        try cwd.copyFile(
            entry_path,
            cwd,
            target,
            .{},
        );
    }
}
