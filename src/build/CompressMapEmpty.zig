pub const CompressHeader = @import("CompressHeader.zig");
const std = @import("std");
const builtin = @import("builtin");

pub const EvalBranchQuotaMultiplier = CompressHeader.EvalBranchQuotaMultiplier;
pub const Method = CompressHeader.Method;
pub const Metadata = CompressHeader.Metadata;
pub const Entry = CompressHeader.Entry;
pub const EntryMap = CompressHeader.EntryMap;

pub const zig_version = builtin.zig_version_string;
pub const os_tag: std.Target.Os.Tag = builtin.os.tag;
pub const EntryType = Entry(Method.Raw, 0);
pub const EntryMapType = EntryMap(EntryType);

pub const map_size = 0;
pub inline fn map() EmptyMap {
    {
        return EmptyMap{};
    }
}

const EmptyMap = struct {
    pub fn get(my_map: EmptyMap, entry: []const u8) ?EntryType {
        _ = my_map;
        _ = entry;
        return null;
    }
};
