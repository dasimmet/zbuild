//!compute zig zon hash
//!

const std = @import("std");
const builtin = @import("builtin");
const RndGen = std.Random.DefaultPrng;

const Options = struct {
    cache_root: []const u8 = "/tmp",
    global_cache_root: []const u8 = "/tmp",
};

pub fn fromUrl(allocator: std.mem.Allocator, url: []const u8) ![]const u8 {
    return fromUrlOpt(allocator, url, Options{});
}

pub fn fromUrlOpt(allocator: std.mem.Allocator, url: []const u8, opt: Options) ![]const u8 {
    const zv = try std.process.Child.run(.{
        .allocator = allocator,
        .argv = &[_][]const u8{ "zig", "version" },
    });
    defer allocator.free(zv.stderr);
    defer allocator.free(zv.stdout);
    const cwd = std.fs.cwd();

    std.log.info("Zig: {s}\nUrl: {s}", .{ zv.stdout, url });
    if (std.mem.startsWith(u8, zv.stdout, "0.11.0")) {
        var rng_buf: [16]u8 = undefined;
        const rng_init: u64 = undefined;
        var rng = RndGen.init(rng_init);
        rng.fill(&rng_buf);

        var hex_buf: [32]u8 = undefined;
        const hex = try std.fmt.bufPrint(
            &hex_buf,
            "{}",
            .{std.fmt.fmtSliceHexLower(&rng_buf)},
        );

        var zbzon = try std.fs.path.join(allocator, &[_][]const u8{ opt.cache_root, "tmp", hex, "build.zig.zon" });
        defer allocator.free(zbzon);

        const zb = zbzon[0 .. zbzon.len - ".zon".len];
        const zbdir = zbzon[0 .. zbzon.len - "build.zig.zon".len - std.fs.path.sep_str.len];

        try cwd.makePath(zbdir);
        defer cwd.deleteTree(zbdir) catch {
            std.log.err("Error! Leaving tmp dir: {s}", .{zbdir});
            @panic("Cleanup Error!");
        };

        {
            const zon_tpl =
                \\.{{
                \\  .paths = .{{""}},
                \\  .name = "",
                \\  .version = "0.0.1",
                \\  .dependencies = .{{
                \\      .tmp = .{{
                \\          .url = "{}",
                \\      }}
                \\  }}
                \\}}
                \\
            ;
            try cwd.writeFile(.{
                .sub_path = zb,
                .data = "",
            });

            var fd = try cwd.createFile(zbzon, .{});
            defer fd.close();
            try std.fmt.format(fd.writer(), zon_tpl, .{
                std.zig.fmtEscapes(url),
            });
        }

        const res = try std.process.Child.run(.{
            .argv = &[_][]const u8{
                "zig",
                "build",
                "--cache-dir",
                opt.cache_root,
                "--global-cache-dir",
                opt.global_cache_root,
            },
            .allocator = allocator,
            .cwd = zbdir,
        });
        allocator.free(res.stdout);
        defer allocator.free(res.stderr);
        const stdout = res.stderr;

        const prefix = "expected .hash = \"";
        var slice = stdout[std.mem.indexOf(u8, stdout, prefix).? + prefix.len ..];
        slice = slice[0..std.mem.indexOf(u8, slice, "\"").?];
        return allocator.dupe(u8, slice);
    } else {
        const res = try std.process.Child.run(.{
            .argv = &[_][]const u8{
                "zig",
                "fetch",
                url,
            },
            .allocator = allocator,
        });
        defer allocator.free(res.stdout);
        defer allocator.free(res.stderr);
        switch (res.term) {
            .Exited => |t| {
                if (t != 0) {
                    std.log.err("StdErr: {s}", .{res.stderr});
                    return error.ErrorCode;
                }
            },
            else => {
                std.log.err("StdErr: {s}", .{res.stderr});
                return error.Term;
            },
        }
        return allocator.dupe(u8, std.mem.trim(u8, res.stdout, &std.ascii.whitespace));
    }
}
