//! The RecusiveDirIterator executes an `entryFn`
//! for each file in a directory by
//! recursively walking a base directory path

const std = @import("std");
const RecursiveDirIterator = @This();
const PathArray = std.BoundedArray(u8, std.fs.max_path_bytes);

/// runs the iterator on directory `base`
pub fn run(
    base: []const u8,
    entryFn: anytype,
    ctx: anytype,
) !void {
    var p = try PathArray.init(0);
    try p.appendSlice(base);

    return iter(
        base,
        entryFn,
        &p,
        ctx,
    );
}

fn iter(
    base: []const u8,
    entryFn: anytype, // Function Pointer to run on all files
    path: *PathArray,
    ctx: anytype,
) !void {
    // std.log.debug("Iterating: {s},{s}", .{ base, path.items});
    var fd = std.fs.cwd().openDir(
        path.slice(),
        .{
            .access_sub_paths = true,
            .no_follow = true,
        },
    ) catch |err| {
        switch (err) {
            error.NotDir => return,
            else => return err,
        }
    };

    defer fd.close();
    var fd_iter = try fd.openDir(".", .{
        .no_follow = true,
        .iterate = true,
    });
    defer fd_iter.close();

    var dir_iter = fd_iter.iterate();
    while (try dir_iter.next()) |entry| {
        try path.appendSlice(std.fs.path.sep_str);
        try path.appendSlice(entry.name);
        switch (entry.kind) {
            .directory,
            .sym_link,
            => {
                try RecursiveDirIterator.iter(
                    base,
                    entryFn,
                    path,
                    ctx,
                );
            },
            .file => {
                try entryFn(base, path.slice(), ctx);
            },
            else => {
                return error.NOTSUPPORTED;
            },
        }
        try path.resize(path.slice().len - entry.name.len - std.fs.path.sep_str.len);
    }
}

/// an example of a function to be passed to the iterator.
/// The ctx argument is passed down from the iterator to entryFn.
pub fn entryFnExample(base: []const u8, entry_path: []const u8, ctx: anytype) !void {
    std.log.info("Entry: {s},{s},{s},{any}", .{ base, entry_path, ctx });
}
