const std = @import("std");
const fmt = @import("../fmt.zig");
const Proc = @import("../Proc.zig");

pub const urls = .{
    .{
        .host = "https://gitlab.com",
        .tpl = "{host}/{prefix}/{name}/-/archive/{ref}/{name}.tar.gz",
        .raw_tpl = "{host}/{prefix}/{name}/-/raw/{ref}/{path}",
        .aliases = .{
            "ssh://git@gitlab.com/",
            "git@gitlab.com:",
        },
    },
    .{
        .host = "https://github.com",
        .tpl = "{host}/{prefix}/{name}/archive/{ref}.tar.gz",
        .raw_tpl = "https://raw.githubusercontent.com/{prefix}/{name}/{ref}/{path}",
        .aliases = .{
            "https://github.com",
            "ssh://git@github.com",
            "git@github.com:",
        },
    },
    .{
        .host = "https://git.sr.ht",
        .tpl = "{host}/{prefix}/{name}/archive/{ref}.tar.gz",
        .aliases = .{
            "ssh://git@git.sr.ht",
            "git@git.sr.ht:",
        },
    },
    .{
        .host = "https://kernel.googlesource.com",
        .tpl = "{host}/{prefix}/{name}.git/+archive/{ref}.tar.gz",
        .aliases = .{
            "https://git.kernel.org",
            "ssh://git.kernel.org",
            "git://git.kernel.org:",
            "https://kernel.googlesource.com",
        },
    },
};

pub fn printRaw(
    writer: anytype,
    args: anytype,
) !void {
    const base = try url_basename(args.url);
    const name = base[0];
    const prefix = base[1];

    inline for (urls) |u| {
        if (@hasField(@TypeOf(u), "raw_tpl")) {
            if (std.mem.startsWith(u8, args.url, u.host)) {
                return fmt.format(u.raw_tpl, writer, .{
                    .host = u.host,
                    .prefix = prefix,
                    .name = name,
                    .url = args.url,
                    .ref = args.ref,
                    .path = args.path,
                });
            }
            inline for (u.aliases) |al| {
                if (std.mem.startsWith(u8, args.url, al)) {
                    return fmt.format(u.raw_tpl, writer, .{
                        .host = u.host,
                        .prefix = prefix,
                        .name = name,
                        .url = args.url,
                        .ref = args.ref,
                        .path = args.path,
                    });
                }
            }
        }
    }
}

pub fn url_basename(raw_url: []const u8) !struct { []const u8, []const u8 } {
    const url = try std.Uri.parse(raw_url);
    var path = std.mem.splitBackwardsScalar(u8, url.path.percent_encoded, '/');
    var name: []const u8 = path.next().?;
    if (std.mem.endsWith(u8, name, ".git")) {
        name = name[0 .. name.len - ".git".len];
    }
    const prefix = url.path.percent_encoded[0 .. @intFromPtr(name.ptr) - @intFromPtr(url.path.percent_encoded.ptr)];
    return .{ name, std.mem.trim(u8, prefix, "/") };
}

pub fn print(
    writer: anytype,
    args: anytype,
) !void {
    const base = try url_basename(args.url);
    const name = base[0];
    const prefix = base[1];

    inline for (urls) |u| {
        if (std.mem.startsWith(u8, args.url, u.host)) {
            const fmt_args = .{
                .host = u.host,
                .name = name,
                .ref = args.ref,
                .prefix = prefix,
            };
            return fmt.format(u.tpl, writer, fmt_args);
        }

        inline for (u.aliases) |al| {
            if (std.mem.startsWith(u8, args.url, al)) {
                const fmt_args = .{
                    .host = u.host,
                    .name = name,
                    .ref = args.ref,
                    .prefix = prefix,
                };
                return fmt.format(u.tpl, writer, fmt_args);
            }
        }
    }
    std.log.err("Error TODO, url not suppurted in {s}: {s}", .{ @src().file, args.url });
    return error.TODO;
}

pub fn from_repo(allocator: std.mem.Allocator, path: []const u8) ![]const u8 {
    const hash = try Proc.git_rev(allocator, path);
    const url = try Proc.git_url(allocator, path);
    var buf = try std.ArrayList(u8).initCapacity(allocator, url.len);

    try print(buf.writer(), .{
        .url = url,
        .ref = hash,
    });
    return buf.toOwnedSlice();
}

pub fn from_remote(
    allocator: std.mem.Allocator,
    url: []const u8,
    ref: []const u8,
) ![]const u8 {
    const branch_name = try std.mem.join(
        allocator,
        "/",
        &[_][]const u8{ "refs", "heads", ref },
    );
    defer allocator.free(branch_name);
    const tag_name = try std.mem.join(
        allocator,
        "/",
        &[_][]const u8{ "refs", "tags", ref },
    );
    defer allocator.free(tag_name);

    const ls_remote = try Proc.ls_remote(allocator, url);
    var line_it = std.mem.splitScalar(
        u8,
        ls_remote,
        '\n',
    );
    const hash = hash: {
        while (line_it.next()) |line| {
            var row_tok = std.mem.tokenizeAny(
                u8,
                line,
                &std.ascii.whitespace,
            );
            var i: usize = 0;
            var tmp_hash: []const u8 = "";
            while (row_tok.next()) |tok| {
                if (i == 0) tmp_hash = std.mem.trim(
                    u8,
                    tok,
                    &std.ascii.whitespace,
                );
                if (i == 1) {
                    const tmp_ref = std.mem.trim(
                        u8,
                        tok[0..],
                        &std.ascii.whitespace,
                    );
                    if (std.mem.eql(u8, tmp_ref, branch_name)) break :hash tmp_hash;
                    if (std.mem.eql(u8, tmp_ref, tag_name)) break :hash tmp_hash;
                }
                i += 1;
            }
        }
        return error.RefNotFound;
    };
    var buf = try std.ArrayList(u8).initCapacity(
        allocator,
        url.len,
    );

    try print(buf.writer(), .{
        .url = url,
        .ref = hash,
    });
    return buf.toOwnedSlice();
}
