//! A `zig build` Step for downloading and installing an Sdk from a zig package
//! example:
//! [Emsdk](https://github.com/emscripten-core/emsdk)

pub const Sdk = @This();
const Self = Sdk;
const std = @import("std");
const ExecutableOptions = std.Build.ExecutableOptions;
const builtin = @import("builtin");
const Fetch = @import("Fetch.zig");
const LazyType = @import("../LazyType.zig");
const Proc = @import("../../Proc.zig");
pub const CopyDir = @import("../CopyDir.zig");
pub const base_id: std.Build.Step.Id = .custom;

pub const CommandList = std.ArrayListUnmanaged(SetupCommand);
pub const RelativePath = struct {
    path: []const u8,
    generated: std.Build.GeneratedFile,

    pub fn resolve(self: *RelativePath, allocator: std.mem.Allocator, prefix: []const u8) []const u8 {
        self.generated.path = std.fs.path.join(
            allocator,
            &[_][]const u8{ prefix, self.path },
        ) catch @panic("OOM");

        return self.generated.path.?;
    }
};

pub const SetupContext = struct {
    man: *std.Build.Cache.Manifest,
    prog: std.Progress.Node,
    sdk: *Sdk,
};

name: []const u8,
step: std.Build.Step,
fetch: *Fetch,
output_dir: std.Build.GeneratedFile,
setup_cmd: CommandList,
relative_paths: std.ArrayListUnmanaged(RelativePath) = .{},
setupFn: ?*const fn (*const SetupContext) anyerror!void = null,
cacheFn: ?*const fn (*const SetupContext) anyerror!void = null,
ctx: *anyopaque = undefined,

/// Options for initialization of the step
pub const Options = struct {
    owner: *std.Build,
    url: LazyType.LazyString,
    hash: ?LazyType.LazyString = null,
    name: []const u8,
    copy_source: bool = true,
    ///first argument is an absolute path to the output directory
    ///call during build stage with output_dir populated with a copy
    ///of the package and should use setup the sdk for reusal
    setupFn: ?*const fn (*const SetupContext) anyerror!void = null,
    ///will be called during the hash stage with the manifest
    ///it should update the manifest with all inputs and set the number of units
    cacheFn: ?*const fn (*const SetupContext) anyerror!void = null,
    ///a context pointer available for setupFn
    ctx: *anyopaque = undefined,
};

pub fn init(
    opt: Options,
) *Self {
    const b = opt.owner;
    var step = std.Build.Step.init(.{
        .name = opt.name,
        .owner = b,
        .id = base_id,
        .makeFn = make,
    });

    var fetch = Fetch.init(.{
        .owner = b,
        .url = opt.url,
        .hash = opt.hash,
        .name = std.mem.concat(b.allocator, u8, &[_][]const u8{
            "fetch-",
            opt.name,
        }) catch @panic("OOM"),
    });
    step.dependOn(&fetch.step);

    const self: *Self = b.allocator.create(Self) catch @panic("OOM");
    self.* = .{
        .ctx = opt.ctx,
        .name = opt.name,
        .step = step,
        .fetch = fetch,
        .output_dir = .{ .step = &self.step },
        .setup_cmd = CommandList{},
        .setupFn = opt.setupFn,
        .cacheFn = opt.cacheFn,
    };
    return self;
}

pub const SetupCommand = struct {
    argv: std.ArrayList(LazyType.LazyString),
    owner: *Self,
    pub fn appendArg(self: *SetupCommand, arg: LazyType.LazyString) !void {
        try self.argv.append(arg);
        switch (arg) {
            .generated => |gen| self.owner.step.dependOn(gen),
            else => {},
        }
    }

    pub fn resolveArgs(self: SetupCommand) []const []const u8 {
        var acc = std.ArrayList([]const u8).init(self.owner.step.owner.allocator);
        for (self.argv.items) |arg| {
            acc.append(arg.resolved) catch @panic("OOM");
        }
        return acc.toOwnedSlice() catch @panic("OOM");
    }
};
/// add a process.Child command to run in output_dir
/// after it is populated by the package contents
pub fn setupCmd(self: *Self, argv: []const []const u8) *SetupCommand {
    var cmd: SetupCommand = .{
        .argv = std.ArrayList(LazyType.LazyString).init(self.step.owner.allocator),
        .owner = self,
    };
    for (argv) |it| {
        cmd.appendArg(.{ .resolved = it }) catch @panic("OOM");
    }

    self.setup_cmd.append(self.step.owner.allocator, cmd) catch @panic("OOM");
    return &self.setup_cmd.items[self.setup_cmd.items.len - 1];
}

/// will return a LazyPath relative to the output_dir resolved by the same step
pub fn relativePath(self: *Self, path: []const u8) std.Build.LazyPath {
    self.relative_paths.append(
        self.step.owner.allocator,
        .{
            .path = path,
            .generated = .{
                .step = &self.step,
            },
        },
    ) catch @panic("OOM");

    return .{
        .generated = .{
            .file = &self.relative_paths.items[self.relative_paths.items.len - 1].generated,
        },
    };
}

fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    const b = step.owner;
    const cwd = std.fs.cwd();
    const stdout = std.io.getStdOut().writer();
    var self = step.cast(Self).?;
    opt.progress_node.increaseEstimatedTotalItems(1);

    var man = b.graph.cache.obtain();
    defer man.deinit();

    man.hash.addBytes(self.fetch.hash.?.resolve());
    if (self.setupFn) |_| {
        opt.progress_node.increaseEstimatedTotalItems(1);
    }

    if (self.cacheFn) |cacheFn| {
        try cacheFn(&.{
            .man = &man,
            .prog = opt.progress_node,
            .sdk = self,
        });
    }

    for (self.setup_cmd.items) |cmd| {
        man.hash.add(cmd.argv.items.len);
        for (cmd.argv.items) |i| {
            opt.progress_node.increaseEstimatedTotalItems(1);
            man.hash.add(@as(u8, 0));
            man.hash.addBytes(i.resolved);
        }
        man.hash.add(@as(u8, '\n'));
    }

    self.step.result_cached = try man.hit();
    const digest = man.final();
    const output_dir = try b.cache_root.join(b.allocator, &[_][]const u8{
        "sdk",
        self.name,
        &digest,
    });
    self.output_dir.path = output_dir;

    if (b.verbose) try std.fmt.format(
        stdout,
        "Sdk \"{s}\" Path: {s}\n",
        .{ self.name, output_dir },
    );

    for (self.relative_paths.items) |*it| {
        const rel = it.resolve(b.allocator, output_dir);

        if (b.verbose) try std.fmt.format(
            stdout,
            "Relative: \"{s}\" Path: {s}\n",
            .{ self.name, rel },
        );
    }

    if (self.step.result_cached) return;
    try cwd.makePath(output_dir);

    {
        if (self.setupFn) |setupFn| {
            if (b.verbose) try std.fmt.format(
                stdout,
                "Sdk \"{s}\" calling setupFn...\n",
                .{self.name},
            );
            // prog_node.setUnit("setupFn");
            try setupFn(&.{
                .man = &man,
                .prog = opt.progress_node,
                .sdk = self,
            });
            opt.progress_node.completeOne();
        }
        for (self.setup_cmd.items) |argv| {
            const args = argv.resolveArgs();
            const printArgs = try std.mem.join(self.step.owner.allocator, " ", args);
            defer self.step.owner.allocator.free(printArgs);
            if (b.verbose) {
                try std.fmt.format(stdout, "Sdk: \"{s}\" CMD: {s}\n", .{ self.name, printArgs });
            }
            // prog_node.setUnit(printArgs);
            const sub_stdout = try Proc.check_output(.{
                .allocator = b.allocator,
                .argv = args,
                .cwd = output_dir,
            });
            defer b.allocator.free(sub_stdout);
            if (b.verbose) _ = try stdout.writeAll(sub_stdout);
            opt.progress_node.completeOne();
        }
    }
    try man.writeManifest();
}

pub fn zigCCEnv(b: *std.Build) !std.process.EnvMap {
    var env = try std.process.getEnvMap(b.allocator);
    {
        const zig_cc = b.fmt("{s} {s}", .{
            b.graph.zig_exe[0..],
            "cc",
        });
        const zig_cxx = b.fmt("{s} {s}", .{
            b.graph.zig_exe[0..],
            "c++",
        });

        try env.put("CC", zig_cc);
        try env.put("CPP", zig_cc);
        try env.put("CXX", zig_cxx);
    }
    return env;
}
