//! A `zig build` Step for downloading and installing
//! [Emsdk](https://github.com/emscripten-core/emsdk)

pub const Emsdk = @This();
const Self = Emsdk;
const std = @import("std");
const ExecutableOptions = std.Build.ExecutableOptions;
const builtin = @import("builtin");
const Sdk = @import("Sdk.zig");
const LazyType = @import("../LazyType.zig");
const Proc = @import("../../Proc.zig");
pub const CopyDir = @import("../CopyDir.zig");
pub const LATEST_EMSDK_VERSION = "3.1.56";
version: ?LazyType.LazyString = null,
libs: ?[]const []const u8 = null,

/// Options for initialization of the step
pub const Options = struct {
    owner: *std.Build,
    name: []const u8,
    url: ?LazyType.LazyString = null,
    hash: ?LazyType.LazyString = null,
    version: ?LazyType.LazyString = null,
    libs: ?[]const []const u8 = null,
};

pub fn init(
    opt: Options,
) *Sdk {
    const b = opt.owner;
    const self = b.allocator.create(Self) catch @panic("OOM");
    self.* = .{
        .version = opt.version,
        .libs = opt.libs,
    };
    return Sdk.init(.{
        .ctx = self,
        .name = opt.name,
        .owner = b,
        .url = opt.url orelse .{
            .resolved = "https://github.com/emscripten-core/emsdk/archive/refs/tags/" ++ LATEST_EMSDK_VERSION ++ ".tar.gz",
        },
        .hash = opt.hash orelse .{
            .resolved = "1220c2ec0db74af2a22679013df77769a0e6bbe184565d021178499fac1427afab6f",
        },
        .cacheFn = cacheFn,
        .setupFn = setupFn,
    });
}

pub fn link(sdk: *Sdk, compile: *std.Build.Step.Compile) void {
    compile.addLibraryPath(sdk.relativePath("upstream/emscripten/cache/sysroot/lib/wasm32-emscripten"));
    compile.addSystemIncludePath(sdk.relativePath("upstream/emscripten/cache/sysroot/include"));
}

pub fn fromSdk(sdk: *Sdk) *Self {
    return @ptrCast(@alignCast(sdk.ctx));
}

fn cacheFn(ctx: *const Sdk.SetupContext) !void {
    const self = fromSdk(ctx.sdk);
    _ = try ctx.man.addFile(@src().file, null);
    const v = if (self.version) |v| v.resolved else LATEST_EMSDK_VERSION;
    ctx.man.hash.addBytes(v);
    ctx.prog.increaseEstimatedTotalItems(2);
    if (self.libs) |libs| {
        for (libs) |lib| {
            ctx.man.hash.addBytes(lib);
            ctx.prog.increaseEstimatedTotalItems(1);
        }
    }
}
fn setupFn(ctx: *const Sdk.SetupContext) !void {
    const b = ctx.sdk.step.owner;
    const self = fromSdk(ctx.sdk);
    if (builtin.os.tag != .windows) {
        _ = try Proc.call(.{
            .allocator = b.allocator,
            .cwd = ctx.sdk.output_dir.getPath(),
            .argv = &[_][]const u8{
                "chmod",
                "+x",
                "emsdk",
            },
            .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
            .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
        });
    }
    // ctx.prog.setUnit("install");
    _ = try Proc.call(.{
        .allocator = b.allocator,
        .cwd = ctx.sdk.output_dir.getPath(),
        .argv = &(emsdk_bin ++ [_][]const u8{
            "install",
            if (self.version) |v| v.resolved else LATEST_EMSDK_VERSION,
        }),
        .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
        .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
    });
    ctx.prog.completeOne();
    // ctx.prog.setUnit("activate");
    _ = try Proc.call(.{
        .allocator = b.allocator,
        .cwd = ctx.sdk.output_dir.getPath(),
        .argv = &(emsdk_bin ++ [_][]const u8{
            "activate",
            if (self.version) |v| v.resolved else LATEST_EMSDK_VERSION,
        }),
        .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
        .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
        .expect_exitcode = 0,
    });
    ctx.prog.completeOne();
    if (self.libs) |libs| {
        for (libs) |lib| {
            // ctx.prog.setUnit(lib);
            _ = try Proc.call(.{
                .allocator = b.allocator,
                .cwd = ctx.sdk.output_dir.getPath(),
                .argv = &[_][]const u8{
                    "upstream/emscripten/embuilder",
                    "build",
                    lib,
                },
                .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
                .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
                .expect_exitcode = 0,
            });
            ctx.prog.completeOne();
        }
    }
}

pub const emsdk_bin = [_][]const u8{
    if (builtin.os.tag == .windows) "cmd" else "sh",
    if (builtin.os.tag == .windows) "/c" else "--",
    if (builtin.os.tag == .windows) "emsdk.bat" else "emsdk",
};
