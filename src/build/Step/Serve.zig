//! A `zig build` Step to serve a directory over http
//! by compressing it into the server's memory

pub const Serve = @This();
const Self = Serve;
const std = @import("std");
const ExecutableOptions = std.Build.ExecutableOptions;
const builtin = @import("builtin");

step: std.Build.Step,
assets: ?*std.Build.Module,
compile: *std.Build.Step.Compile,
public_folder: ?std.Build.LazyPath = null,
port: u16 = 0,

/// Options for initialization of the step
pub const Options = struct {
    owner: *std.Build,
    /// module output of a Compress Step.
    /// The map() is used to serve static files
    assets: ?*std.Build.Module = null,
    name: []const u8,
    /// will be duped internally and passed to addExecutable.
    /// root_source_file will be overridden by the Server entrypoint.
    options: ExecutableOptions,
    /// a module to handle requests not covered by assets
    /// this should Mirror the decls of ZBuild.Srv.Api
    api: ?*std.Build.Module = null,

    /// a runtime folder to serve from,
    public_folder: ?std.Build.LazyPath = null,

    /// default port, 0 means os picks one
    port: u16 = 0,
};
// initialize the library and pass down dependencies
pub fn init(
    opt: Options,
) *Self {
    const b = opt.owner;
    var exe_opt: ExecutableOptions = opt.options;

    // this guarantees a relative path to the current file
    exe_opt.root_source_file = .{
        .cwd_relative = b.pathJoin(&[_][]const u8{
            @src().file,
            "..",
            "..",
            "..",
            "srv",
            "main.zig",
        }),
    };
    const mod: *std.Build.Module = if (opt.api) |api| api else b.addModule("ZBuild.Srv.Api", .{
        .root_source_file = .{ .cwd_relative = b.pathJoin(&[_][]const u8{
            @src().file,
            "..",
            "..",
            "..",
            "srv",
            "Api.zig",
        }) },
    });

    var step = std.Build.Step.init(.{
        .name = opt.name,
        .owner = b,
        .id = std.Build.Step.Id.custom,
        .makeFn = make,
    });

    const self: *Self = b.allocator.create(Self) catch @panic("OOM");

    const zap = b.lazyDependency(
        "zap",
        .{
            .target = exe_opt.target orelse b.graph.host,
            .optimize = exe_opt.optimize,
        },
    );
    const exe = b.addExecutable(exe_opt);
    if (zap) |zap_dep| {
        const zap_module = zap_dep.module("zap");
        exe.root_module.addImport("zap", zap_module);
        mod.addImport("zap", zap_module);
        exe.linkLibrary(zap_dep.artifact("facil.io"));
    }

    if (opt.assets) |assets_mod| {
        exe.root_module.addImport("ZBuild.Srv.Assets", assets_mod);
    } else {
        exe.root_module.addAnonymousImport("ZBuild.Srv.Assets", .{
            .root_source_file = b.path(b.pathJoin(&[_][]const u8{
                @src().file,
                "..",
                "..",
                "CompressMapEmpty.zig",
            })),
        });
    }

    const options = b.addOptions();
    if (opt.public_folder) |public_folder| {
        options.addOptionPath("public_folder", public_folder);
    }
    options.addOption(u16, "port", opt.port);
    exe.root_module.addOptions("ZBuild.Srv.Options", options);

    exe.root_module.addImport("ZBuild.Srv.Api", mod);
    step.dependOn(&exe.step);

    self.* = .{
        .step = step,
        .assets = opt.assets,
        .compile = exe,
        .public_folder = opt.public_folder,
        .port = opt.port,
    };
    return self;
}

fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    _ = opt;
    _ = step;
}

pub fn run(
    self: *Self,
    b: *std.Build,
) *std.Build.Step.Run {
    const run_art = b.addRunArtifact(self.compile);
    if (b.args) |args| {
        run_art.addArgs(args);
    }
    if (self.public_folder) |public_folder| {
        switch (public_folder) {
            .generated => |pf| {
                run_art.step.dependOn(pf.file.step);
            },
            else => {},
        }
    }
    return run_art;
}
