const ZBuild = @import("../../ZBuild.zig");
const std = @import("std");
const LazyPath = std.Build.LazyPath;
const GeneratedFile = std.Build.GeneratedFile;
const builtin = @import("builtin");
const exe = builtin.target.exeFileExt();

const AndroidSdk = @This();
const Paths = struct {
    aapt: LazyPath,
    adb: LazyPath,
    apksigner: LazyPath,
    sdkmanager: LazyPath,
    zipalign: LazyPath,
};
const Files = struct {
    aapt: *GeneratedFile,
    adb: *GeneratedFile,
    apksigner: *GeneratedFile,
    sdkmanager: *GeneratedFile,
    zipalign: *GeneratedFile,
};

sdk: *ZBuild.Step.Sdk,
build_tools: []const u8 = "35.0.0",
ndk: []const u8 = "28.0.12674087",
platforms: []const u8 = "android-35",
paths: Paths,
files: Files,

pub fn init(b: *std.Build) *AndroidSdk {
    const self = b.allocator.create(AndroidSdk) catch @panic("OOM");
    self.* = .{
        .sdk = ZBuild.Step.Sdk.init(.{
            .owner = b,
            .name = "androidsdk",
            .copy_source = false,
            .url = .{
                .resolved = "https://dl.google.com/android/repository/commandlinetools-" ++ @tagName(builtin.os.tag) ++ "-11076708_latest.zip",
            },
            .hash = .{ .resolved = "1220afe532bcf5e077918c0cc5e885ba3e8a7d2e12a013017f48040c25dfb6301291" },
            .cacheFn = AndroidSdk.cache,
            .setupFn = AndroidSdk.setup,
            .ctx = self,
        }),
        .paths = undefined,
        .files = undefined,
    };
    inline for (std.meta.fields(Paths)) |f| {
        const gf: *std.Build.GeneratedFile = b.allocator.create(std.Build.GeneratedFile) catch @panic("OOM");
        gf.* = .{ .step = &self.sdk.step };
        @field(self.files, f.name) = gf;
        @field(self.paths, f.name) = LazyPath{
            .generated = .{
                .file = gf,
            },
        };
    }
    return self;
}

fn cache(ctx: *const ZBuild.Step.Sdk.SetupContext) !void {
    const self: *AndroidSdk = @alignCast(@ptrCast(ctx.sdk.ctx));
    ctx.man.hash.addBytes(self.sdk.fetch.url.resolve());
    ctx.man.hash.addBytes(self.build_tools);
    ctx.man.hash.addBytes(self.ndk);
    ctx.man.hash.addBytes(self.platforms);
    ctx.prog.increaseEstimatedTotalItems(2);
}

fn setup(ctx: *const ZBuild.Step.Sdk.SetupContext) !void {
    const self: *AndroidSdk = @alignCast(@ptrCast(ctx.sdk.ctx));
    const output_dir = ctx.sdk.output_dir.getPath();
    const b = ctx.sdk.step.owner;
    const allocator = b.allocator;

    try unwrapFileNotFound(std.fs.deleteTreeAbsolute(output_dir));

    self.files.aapt.path = b.pathJoin(&.{
        self.sdk.output_dir.getPath(),
        "build-tools",
        self.build_tools,
        "aapt" ++ exe,
    });
    self.files.adb.path = b.pathJoin(&.{
        self.sdk.output_dir.getPath(),
        "platform-tools",
        "adb" ++ exe,
    });
    self.files.apksigner.path = b.pathJoin(&.{
        self.sdk.output_dir.getPath(),
        "build-tools",
        self.build_tools,
        "apksigner" ++ exe,
    });
    self.files.sdkmanager.path = b.pathJoin(&.{
        self.sdk.fetch.output_dir.getPath(),
        "bin",
        "sdkmanager" ++ exe,
    });
    self.files.zipalign.path = b.pathJoin(&.{
        self.sdk.output_dir.getPath(),
        "build-tools",
        self.build_tools,
        "zipalign" ++ exe,
    });

    const installing_dir = b.fmt("{s}.installing", .{output_dir});
    try unwrapFileNotFound(std.fs.deleteTreeAbsolute(installing_dir));

    const sdk_root_arg = b.fmt("--sdk_root={s}", .{installing_dir});

    if (ctx.sdk.step.owner.verbose) {
        std.log.debug("AndroidSDK Path: {s}\nSdkManager: {s}\n", .{ output_dir, self.paths.sdkmanager.getPath(b) });
    }
    {
        var fd = try std.fs.cwd().openFile(self.paths.sdkmanager.getPath(b), .{});
        defer fd.close();

        try fd.chmod(0o777);
    }
    ctx.prog.completeOne();

    var env = try ZBuild.Step.Sdk.zigCCEnv(b);
    defer env.deinit();

    const sdkmanager_cmd = &[_][]const u8{
        self.paths.sdkmanager.getPath(b),
        sdk_root_arg,
        b.fmt("build-tools;{s}", .{self.build_tools}),
        b.fmt("ndk;{s}", .{self.ndk}),
        "platform-tools",
        b.fmt("platforms;{s}", .{self.platforms}),
        "tools",
    };
    errdefer {
        std.fs.deleteTreeAbsolute(installing_dir) catch {};
    }
    _ = try ZBuild.Proc.call(.{
        .allocator = allocator,
        .argv = sdkmanager_cmd,
        .env_map = &env,
        .stdin_behavior = .Pipe,
        .stdin_bytes = "y\ny\ny\ny\ny\n",
        .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
        .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
        .expect_exitcode = 0,
    });
    try std.fs.renameAbsolute(installing_dir, output_dir);
    ctx.prog.completeOne();
}

fn unwrapFileNotFound(res: anyerror!void) !void {
    return res catch |e| {
        switch (e) {
            error.FileNotFound => return,
            else => return e,
        }
    };
}
