//! A `zig build` Step to Fetch a package at build time
//! needs the `zig fetch` subcommand from 0.12.0
//!

const FetchStep = @This();
const std = @import("std");
const builtin = @import("builtin");
const LazyType = @import("../LazyType.zig");
const Proc = @import("../../Proc.zig");
pub const base_id: std.Build.Step.Id = .custom;

step: std.Build.Step,
url: LazyType.LazyString,
hash: ?LazyType.LazyString,
validate: bool = false,
output_dir: std.Build.GeneratedFile,
/// Maximum size of file operations, default: 1GB
max_file_size: usize = 1073741824,

const Self = FetchStep;
pub const Options = struct {
    owner: *std.Build,
    url: LazyType.LazyString,
    hash: ?LazyType.LazyString = null,
    validate: bool = false,
    name: []const u8,
};

pub fn init(opt: Options) *Self {
    var step = std.Build.Step.init(.{
        .name = opt.name,
        .owner = opt.owner,
        .id = base_id,
        .makeFn = make,
    });

    opt.url.addStepDependencies(&step);

    const self: *@This() = opt.owner.allocator.create(@This()) catch {
        @panic("Alloc Error");
    };

    self.* = .{
        .step = step,
        .url = opt.url,
        .hash = opt.hash,
        .validate = opt.validate,
        .output_dir = .{ .step = &self.step },
    };
    return self;
}

fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    _ = opt;
    const b = step.owner;
    const allocator = b.allocator;
    var self = step.cast(Self).?;
    const stdout = std.io.getStdOut().writer();

    const url = self.url.resolve();

    var man = b.graph.cache.obtain();
    defer man.deinit();

    man.hash.addBytes(b.graph.zig_exe);
    man.hash.addBytes(url);
    man.hash.addBytes(b.graph.global_cache_root.path.?);

    const argv = [_][]const u8{
        b.graph.zig_exe,
        "fetch",
        "--global-cache-dir",
        b.graph.global_cache_root.path orelse return error.NoGlobalCacheFound,
        url,
    };

    if (self.hash == null) {
        const zig_stdout = try Proc.check_output(.{
            .allocator = allocator,
            .argv = &argv,
        });
        defer allocator.free(zig_stdout);
        const res_hash_trim = std.mem.trim(u8, zig_stdout, " \n\r\t");

        try std.fmt.format(
            stdout,
            "No Hash provided to Step. zig fetch returned:\n.hash = .{{ .resolved = \"{s}\" }},\n",
            .{res_hash_trim},
        );
        return error.MissingHash;
    }
    const hash = self.hash.?.resolve();
    man.hash.addBytes(hash);

    const output_dir = try b.graph.global_cache_root.join(
        allocator,
        &.{
            "p", hash,
        },
    );

    man.want_shared_lock = true;
    self.step.result_cached = try man.hit();
    // var out_dir_fd: ?std.fs.Dir = std.fs.openDirAbsolute(
    //     output_dir,
    //     .{},
    // ) catch |err|
    //     switch (err) {
    //     error.FileNotFound => blk: {
    //         if (self.step.result_cached) man.manifest_dirty = true;
    //         self.step.result_cached = false;
    //         break :blk @as(?std.fs.Dir, null);
    //     },
    //     else => return err,
    // };
    // if (out_dir_fd) |*fd| fd.close();

    if (b.verbose) try std.fmt.format(
        stdout,
        "Fetch:\nurl:{s}\noutput_dir:{s}\n",
        .{ url, output_dir },
    );

    self.output_dir.path = output_dir;
    if ((!self.validate) and self.step.result_cached) return;

    const zig_stdout = try Proc.check_output(.{
        .allocator = allocator,
        .argv = &argv,
    });
    defer allocator.free(zig_stdout);
    const res_hash_trim = std.mem.trim(
        u8,
        zig_stdout,
        " \n\r\t",
    );

    if (!std.mem.eql(u8, hash, res_hash_trim)) {
        try std.fmt.format(
            stdout,
            "Hash mismatch. zig fetch returned:\n.hash = .{{ .resolved = \"{s}\" }},\nStep config has:\n.hash = .{{ .resolved = \"{s}\" }},\n",
            .{ res_hash_trim, hash },
        );
        return error.HashMismatch;
    }

    try man.writeManifest();
}
