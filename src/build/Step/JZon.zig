//! A `zig build` Step to convert a json file to zig code
//!

pub const JZon = Self;
const Self = @This();
const std = @import("std");
const builtin = @import("builtin");
pub const JsonToZon = @import("../JsonToZon.zig");
pub const JsonToZonLiteral = @embedFile("../JsonToZon.zig");
pub const base_id: std.Build.Step.Id = .custom;

step: std.Build.Step,
source_file: std.Build.LazyPath,
output_file: std.Build.GeneratedFile,
max_file_size: usize = 1073741824,
/// Indentation of the generated ZON. Defaults to 4 like zig fmt
indent: u8 = 4,
zon: bool = false,

// create a step to convert source_file to output_file
pub fn init(
    b: *std.Build,
    source_file: std.Build.LazyPath,
    name: []const u8,
) *Self {
    var step = std.Build.Step.init(.{
        .name = name,
        .owner = b,
        .id = base_id,
        .makeFn = make,
    });

    if (source_file == .generated) {
        step.dependOn(source_file.generated.file.step);
    }
    source_file.addStepDependencies(&step);

    const self: *Self = b.allocator.create(Self) catch {
        @panic("Alloc Error");
    };

    self.* = .{
        .step = step,
        .source_file = source_file,
        .output_file = .{ .step = &self.step },
    };
    return self;
}

// run the build step
fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    _ = opt;
    const b = step.owner;
    const allocator = b.allocator;
    var self = step.cast(Self).?;

    const source_file = self.source_file.getPath2(
        b,
        step,
    );
    const basename = std.fs.path.basename(source_file);
    const suffix = if (self.zon) ".zon" else ".zig";
    const out_basename = try std.mem.join(
        allocator,
        "",
        &[_][]const u8{ basename, suffix },
    );
    defer allocator.free(out_basename);

    var man = b.graph.cache.obtain();
    defer man.deinit();

    _ = try man.addFile(source_file, self.max_file_size);
    man.hash.addBytes(JsonToZonLiteral);
    man.hash.add(self.zon);

    const cached = try man.hit();
    self.step.result_cached = cached;
    const digest = man.final();

    const output_dir = try b.cache_root.join(allocator, &.{
        "o", &digest,
    });
    defer allocator.free(output_dir);

    self.output_file.path = try b.cache_root.join(allocator, &.{
        "o", &digest, out_basename,
    });

    if (b.verbose) {
        std.log.info("JZon: {s}", .{self.output_file.path.?});
    }
    if (cached) return;

    const parsed = parsed: {
        const content = try std.fs.cwd().readFileAlloc(
            allocator,
            source_file,
            self.max_file_size,
        );
        defer allocator.free(content);

        break :parsed try std.json.parseFromSlice(
            std.json.Value,
            allocator,
            content,
            .{},
        );
    };
    defer parsed.deinit();

    try std.fs.cwd().makePath(output_dir);
    const out_file = try std.fs.createFileAbsolute(
        self.output_file.path.?,
        .{},
    );
    defer out_file.close();
    if (self.zon) {
        try JsonToZon.writeZon(
            parsed.value,
            out_file.writer(),
            self.indent,
        );
    } else {
        try JsonToZon.writeZig(
            parsed.value,
            out_file.writer(),
            self.indent,
        );
    }
}

/// returns a `std.Build.Module` of `output_file`
pub fn module(self: *Self, b: *std.Build) *std.Build.Module {
    return b.addModule(self.step.name, .{
        .root_source_file = .{
            .generated = .{
                .file = &self.output_file,
            },
        },
    });
}
