//! A `zig build` Step to download a file via http
//!

const DownloadStep = @This();
const std = @import("std");
const builtin = @import("builtin");
const LazyType = @import("../LazyType.zig");
pub const base_id: std.Build.Step.Id = .custom;

step: std.Build.Step,
url: LazyType.LazyString,
output_dir: std.Build.GeneratedFile,
output_file: std.Build.GeneratedFile,
/// Maximum size of file operations, default: 1GB
max_file_size: usize = 1073741824,
/// store results in a global cache root
shared: bool = false,

const Self = DownloadStep;

pub fn init(
    b: *std.Build,
    url: LazyType.LazyString,
    name: []const u8,
) *Self {
    var step = std.Build.Step.init(.{
        .name = name,
        .owner = b,
        .id = base_id,
        .makeFn = make,
    });

    url.addStepDependencies(&step);

    const self: *@This() = b.allocator.create(@This()) catch {
        @panic("Alloc Error");
    };

    self.* = .{
        .step = step,
        .url = url,
        .output_dir = .{ .step = &self.step },
        .output_file = .{ .step = &self.step },
    };
    return self;
}

fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    _ = opt;
    const b = step.owner;
    const allocator = b.allocator;
    var self = step.cast(Self).?;

    const url = self.url.resolve();

    var man = b.graph.cache.obtain();
    defer man.deinit();

    _ = try man.addFile(
        @src().file,
        self.max_file_size,
    );
    man.hash.addBytes(url);
    man.hash.add(self.shared);

    const basename = std.fs.path.basename(url);

    self.step.result_cached = try man.hit();
    const digest = man.final();

    const cache_dir = if (self.shared)
        b.graph.global_cache_root
    else
        b.cache_root;

    self.output_dir.path = try cache_dir.join(allocator, &.{
        "o", &digest,
    });

    self.output_file.path = try cache_dir.join(allocator, &.{
        "o", &digest, basename,
    });
    // std.log.warn("out: {s}", .{self.output_file.path.?});

    try cache_dir.handle.makePath("o");

    try std.fs.cwd().makePath(self.output_dir.path.?);

    if (self.step.result_cached) return;
    {
        try cache_dir.handle.makePath("tmp");

        const dl_dir = try cache_dir.join(
            allocator,
            &.{
                "tmp", &digest,
            },
        );
        defer allocator.free(dl_dir);

        const dl_file = try cache_dir.join(
            allocator,
            &.{
                "tmp", &digest, basename,
            },
        );
        defer allocator.free(dl_file);
        if (b.verbose)
            std.log.info("Downloading\nurl: {s}\ntmp: {s}\nto: {s}\n", .{
                url,
                dl_file,
                self.output_file.path.?,
            });

        // const cwd = std.fs.cwd();
        try std.fs.cwd().makePath(dl_dir);
        defer std.fs.deleteTreeAbsolute(dl_dir) catch {
            std.log.err("Error Cleaning Dir: {s}", .{dl_dir});
            @panic("Directory Cleanup");
        };

        {
            const uri = std.Uri.parse(url) catch |err| {
                std.log.err("invalid URI: {s}", .{@errorName(err)});
                return err;
            };

            var http_client = std.http.Client{
                .allocator = b.allocator,
            };

            const hbuf = try b.allocator.alloc(u8, 16 * 1024);
            defer b.allocator.free(hbuf);

            var req = http_client.open(
                .GET,
                uri,
                .{ .server_header_buffer = hbuf },
            ) catch |err| {
                std.log.err(
                    "unable to connect to server: {s}",
                    .{@errorName(err)},
                );
                return err;
            };
            errdefer req.deinit(); // releases more than memory

            req.send() catch |err| {
                std.log.err(
                    "HTTP request failed: {s}",
                    .{@errorName(err)},
                );
                return err;
            };
            req.wait() catch |err| {
                std.log.err(
                    "invalid HTTP response: {s}",
                    .{@errorName(err)},
                );
                return err;
            };

            if (req.response.status != .ok) {
                std.log.err(
                    "bad HTTP response code: '{d} {s}'",
                    .{ @intFromEnum(req.response.status), req.response.status.phrase() orelse "" },
                );
                return error.NOT_OK;
            }
            var buf: [4096]u8 = undefined;
            var fd = try std.fs.createFileAbsolute(
                dl_file,
                .{},
            );
            defer fd.close();
            var count = try req.reader().read(buf[0..]);

            while (count != 0) {
                _ = try fd.writer().write(buf[0..count]);
                count = try req.reader().read(buf[0..]);
            }
        }

        std.fs.renameAbsolute(
            dl_file,
            self.output_file.path.?,
        ) catch |err| {
            std.log.err("Error renaming File: {s}", .{dl_file});
            return err;
        };
        try man.writeManifest();
    }
}
