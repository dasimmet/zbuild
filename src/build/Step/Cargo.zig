//! A `zig build` Step to download a file via http
//!

const DownloadStep = @This();
const std = @import("std");
const builtin = @import("builtin");
const LazyType = @import("../LazyType.zig");
pub const base_id: std.Build.Step.Id = .custom;

step: std.Build.Step,
url: LazyType.LazyString,
output_dir: std.Build.GeneratedFile,
output_file: std.Build.GeneratedFile,
/// Maximum size of file operations, default: 1GB
max_file_size: usize = 1073741824,
/// store results in a global cache root
shared: bool = false,

const Self = DownloadStep;

pub fn init(
    b: *std.Build,
    url: LazyType.LazyString,
    name: []const u8,
) *Self {
    var step = std.Build.Step.init(.{
        .name = name,
        .owner = b,
        .id = base_id,
        .makeFn = make,
    });

    url.addStepDependencies(&step);

    const self: *@This() = b.allocator.create(@This()) catch {
        @panic("Alloc Error");
    };

    self.* = .{
        .step = step,
        .url = url,
        .output_dir = .{ .step = &self.step },
        .output_file = .{ .step = &self.step },
    };
    return self;
}

fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    _ = opt;
    const b = step.owner;
    const allocator = b.allocator;
    const self = step.cast(Self).?;
    _ = self;
    _ = allocator;
}
