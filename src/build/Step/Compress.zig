//! A `zig build` Step to convert a directory of files into a
//! StaticStringMap
//!

const Compress = @This();
const std = @import("std");
const builtin = @import("builtin");

pub const RecursiveDirIterator = @import("../RecursiveDirIterator.zig");
pub const CompressMap = @import("../CompressMap.zig");

pub const base_id: std.Build.Step.Id = .custom;
pub const Method = CompressMap.CompressHeader.Method;
// This Step generates a "compress.zig" source code file
// in "out_file" containing a CompressHeader.zig
// as well as a StaticStringMap "map" with all
// files in the directory tree below "dir".

step: std.Build.Step,
content: std.ArrayList(Content),
// dir: std.Build.LazyPath,
method: Method = .Deflate,
max_file_size: usize = std.math.pow(usize, 2, 30),
embed_full_path: bool = false,
mime_mode: CompressMap.MimeMode = .Any,
/// include stat data
output_file: std.Build.GeneratedFile,
indent: u8 = 0,

/// defined after .module(), .exe() is called
__module: ?*std.Build.Module = null,
__exe: ?*std.Build.Step.Compile = null,

pub const OUT_BASENAME = "assets.zig";
const Self = Compress;
const Options = struct {
    build: *std.Build,
    dir: ?std.Build.LazyPath = null,
    name: []const u8,
    method: Method = .Deflate,
    mime_mode: CompressMap.MimeMode = .Any,
};

const Content = union(enum) {
    Dir: ContentFd,
    File: ContentFd,
};
const ContentFd = struct {
    source_path: std.Build.LazyPath,
    map_path: std.Build.LazyPath,
    method: ?Method = null,
};

pub fn init(opt: Options) *Self {
    const step = std.Build.Step.init(.{
        .name = opt.name,
        .owner = opt.build,
        .id = base_id,
        .makeFn = make,
    });

    const self: *Self = opt.build.allocator.create(Self) catch {
        @panic("Alloc Error");
    };
    const content = std.ArrayList(Content).init(opt.build.allocator);
    self.* = .{
        .step = step,
        .content = content,
        .output_file = .{ .step = &self.step },
        .method = opt.method,
        .mime_mode = opt.mime_mode,
    };
    if (opt.dir) |d| {
        d.addStepDependencies(&self.step);
        self.content.append(.{
            .Dir = .{
                .source_path = d.dupe(opt.build),
                .map_path = self.step.owner.path(""),
            },
        }) catch @panic("OOM");
    }

    return self;
}

pub fn exe(self: *Self, b: *std.Build) *std.Build.Step.Compile {
    if (self.__exe == null) {
        self.__exe = b.addExecutable(.{
            .name = "assets",
            .root_source_file = .{
                .generated = .{ .file = &self.output_file },
            },
            .target = b.resolveTargetQuery(std.Target.Query{}),
        });
    }
    return self.__exe.?;
}

pub fn addFile(self: *Self, f: ContentFd) void {
    f.map_path.addStepDependencies(&self.step);
    f.source_path.addStepDependencies(&self.step);
    self.content.append(.{ .File = f }) catch @panic("OOM");
}

pub fn module(self: *Self, b: *std.Build) *std.Build.Module {
    if (self.__module == null) {
        self.__module = b.addModule(self.step.name, .{
            .root_source_file = .{
                .generated = .{
                    .file = &self.output_file,
                },
            },
        });
    }
    return self.__module.?;
}

fn make(step: *std.Build.Step, opt: std.Build.Step.MakeOptions) anyerror!void {
    const stdout = std.io.getStdOut().writer();
    const b = step.owner;
    const allocator = b.allocator;

    var self = step.cast(Self).?;

    var man = b.graph.cache.obtain();
    defer man.deinit();
    const cwd = std.fs.cwd();

    self.hash(&man);

    var ctx: Context = .{
        .compress = self,
        .map = .{
            .prog = opt.progress_node,
            .entry = .{
                .allocator = allocator,
                .source = "",
                .dest = "",
                .embed_full_path = self.embed_full_path,
                .default_method = self.method,
                .method = self.method,
                .max_file_size = self.max_file_size,
                .mime_mode = self.mime_mode,
                .indent = self.indent,
            },
        },
        .man = &man,
    };
    for (self.content.items) |content| {
        switch (content) {
            .Dir => |c| {
                ctx.map.entry.method = c.method orelse self.method;
                try RecursiveDirIterator.run(
                    c.source_path.getPath2(b, step),
                    hashEntry,
                    &ctx,
                );
            },
            .File => |c| {
                ctx.map.entry.method = c.method orelse self.method;
                ctx.man.hash.addBytes(c.map_path.getPath(b));
                try hashEntry(
                    "",
                    c.source_path.getPath2(b, step),
                    &ctx,
                );
            },
        }
    }
    step.result_cached = try man.hit();
    const digest = man.final();

    self.output_file.path = try b.cache_root.join(allocator, &.{
        "o", &digest, OUT_BASENAME,
    });

    if (b.verbose) {
        try std.fmt.format(
            stdout,
            "CompressFile: {s}\n",
            .{self.output_file.path.?},
        );
    }
    if (step.result_cached) return;

    const out_path = self.output_file.getPath();

    const out_dir = std.fs.path.dirname(out_path).?;
    try cwd.makePath(out_dir);

    var raw_file = try cwd.createFile(out_path, .{});
    defer raw_file.close();
    const fd = raw_file.writer();
    ctx.map.writer = fd.any();

    try CompressMap.header(
        fd,
        ctx.compress.method,
        "application/octet-stream",
        ctx.map.biggest_file,
        ctx.map.count,
    );

    for (self.content.items) |content| {
        switch (content) {
            .Dir => |c| {
                ctx.map.entry.method = c.method orelse self.method;
                try CompressMap.addDir(
                    c.source_path.getPath2(b, &self.step),
                    &ctx.map,
                );
            },
            .File => |c| {
                ctx.map.entry.method = c.method orelse self.method;
                try CompressMap.addFile(
                    c.source_path.getPath2(b, step),
                    c.map_path.getPath2(b, step),
                    &ctx.map,
                );
            },
        }
    }

    try CompressMap.footer(fd);
    try step.writeManifest(&man);
}

fn hash(self: *Compress, man: *std.Build.Cache.Manifest) void {
    man.hash.add(self.method);
    man.hash.add(self.embed_full_path);
    man.hash.add(self.mime_mode);
    man.hash.add(CompressMap.MimeExt.data.len);
    man.hash.add(std.meta.fields(@TypeOf(CompressMap.MimeMagic.data)).len);
    man.hash.addBytes(CompressMap.CompressHeader_literal);
    man.hash.addBytes(CompressMap.HeaderTemplate);
    man.hash.addBytes(builtin.zig_version_string);
    man.hash.addBytes(CompressMap.HeaderTemplateDeps);
}

const Context = struct {
    compress: *Compress,
    map: CompressMap.Context,
    man: *std.Build.Cache.Manifest,
};

const WriteError = error{
    Write,
};

fn hashEntry(
    base: []const u8,
    path: []const u8,
    ctx: *Context,
) !void {
    _ = base;
    try CompressMap.incrementContext("", path, &ctx.map);

    ctx.man.hash.addBytes(path);
    ctx.man.hash.add(ctx.map.entry.method);
    _ = try ctx.man.addFile(path, ctx.compress.max_file_size);
}
