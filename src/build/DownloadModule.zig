//! Creates a Module from a https url
//! by downloading it into the zig cache
//! and optionally copying to source

const Self = @This();
const DownloadModule = Self;
const std = @import("std");
const Download = @import("Step/Download.zig");
const CopyFile = union(enum) {
    toSource: *std.Build.Step.UpdateSourceFiles,
    toCache: *std.Build.Step.WriteFile,
    pub fn step(self: *@This()) *std.Build.Step {
        return switch (self.*) {
            .toSource => |s| &s.step,
            .toCache => |s| &s.step,
        };
    }
};
copy: ?*CopyFile,
exists: Exists = .Dunno,
file: std.Build.LazyPath,
module: *std.Build.Module,

pub const Exists = enum {
    True,
    False,
    Dunno,
};

pub const Options = struct {
    toSource: bool = false,
    owner: *std.Build,
    url: []const u8,
    name: []const u8,
    path: ?[]const u8 = null,
};

/// for a given url creates a `ZBuild.Step.Download` and
/// adds a module with the name.
pub fn init(opt: Options) Self {
    const b = opt.owner;
    const download_name = std.mem.join(
        b.allocator,
        " ",
        &[_][]const u8{ "download ", opt.name },
    ) catch @panic("OOM");
    std.debug.assert(std.mem.endsWith(u8, opt.url, ".zig"));

    const download = Download.init(
        b,
        .{
            .resolved = opt.url,
        },
        download_name,
    );

    const file: std.Build.LazyPath = .{ .generated = .{
        .file = &download.output_file,
    } };

    var copy: ?*CopyFile = null;
    var path_exists: Exists = .Dunno;
    if (opt.path) |p| {
        const resolved = std.fs.path.resolve(
            b.allocator,
            &[_][]const u8{p},
        ) catch @panic("OOM");
        path_exists = .True;
        _ = std.fs.cwd().statFile(resolved) catch |err| {
            if (err != error.FileNotFound) @panic("FS ERROR");
            copy = b.allocator.create(CopyFile) catch @panic("OOM");
            if (opt.toSource) {
                copy.?.* = .{ .toSource = b.addUpdateSourceFiles() };
                copy.?.toSource.addCopyFileToSource(file, resolved);
            } else {
                copy.?.* = .{ .toCache = b.addWriteFiles() };
                _ = copy.?.toCache.addCopyFile(file, resolved);
            }
            // copy = switch (opt.toSource) {
            //     true => b.addUpdateSourceFiles(),
            //     false => b.addWriteFiles(),
            // };
            path_exists = .False;
        };
    }

    const module = b.addModule(opt.name, .{
        .root_source_file = if (path_exists == .True) .{ .cwd_relative = opt.path.? } else file,
    });

    return .{
        .file = file,
        .copy = copy,
        .exists = path_exists,
        .module = module,
    };
}
