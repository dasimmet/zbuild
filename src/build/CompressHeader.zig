//! a generated map of embedded files using string literals.
//! Usage:
//!
//! const assets = @import("this_file_or_module.zig")
//!
//! const e: assets.EntryType = assets.map().get("relative_file_path")
//!
//! for (assets.map().kvs) |kv| {
//!     std.log.debug("{any}", .{kv});
//! }
//!

const std = @import("std");

// this will be multiplied by the number of entries
// and passed to @setEvalBranchQuota to
// allow importing programs to compile the map
// this depends on  the implementation of StaticStringMap
// and was optimized by trial and error
// for zig version "0.12.0-dev.790+ad6f8e3a5"
pub const EvalBranchQuotaMultiplier = 30;

pub const Method = enum {
    Unknown,
    Raw,
    Gzip,
    Deflate,
    XZ,
    ZStd,
};
pub const Metadata = struct {
    size: u128,
    accessed: u128,
    created: ?u128 = null,
    modified: u128,
};

pub fn Entry(comptime method: Method, comptime biggest_file_size: usize) type {
    return struct {
        const Self = @This();
        full_path: ?[]const u8 = null,
        body: []const u8,
        method: Method = method,
        mimetype: []const u8 = "",
        sha256: []const u8,
        md5: []const u8,
        metadata: Metadata,
        biggest_file: usize = biggest_file_size,

        pub fn decompressBodyAlloc(self: Self, allocator: std.mem.Allocator) ![]u8 {
            if (self.method == .Raw) {
                return allocator.dupe(u8, self.body);
            }
            if (std.math.maxInt(usize) < self.metadata.size) return error.BodyTooBig;

            const decom = try self.decompressor(allocator);
            const buf = try allocator.alloc(u8, @intCast(self.metadata.size));
            var count: usize = 0;
            while (count != buf.len) {
                count += try decom.read(buf[count..]);
            }
            return buf;
        }

        pub fn decompressBody(self: Self, writer: anytype, comptime bufsize: usize) !usize {
            if (self.method == .Raw) {
                return writer.write(self.body);
            }

            var buf: [bufsize]u8 = undefined;

            const compressor_bufsize = comptime std.mem.alignForward(usize, biggest_file_size, 1024) / 2;
            var compressor_buf: [compressor_bufsize]u8 = undefined;
            var fba = std.heap.FixedBufferAllocator.init(compressor_buf[0..]);

            const decom = try self.decompressor(fba.allocator());

            const size = @as(usize, @truncate(self.metadata.size));
            var pos: usize = 0;
            while (pos < size) {
                const remainder: usize = size - pos;
                const len = try decom.read(buf[0..@min(remainder, buf.len)]);
                if (len == 0)
                    return error.UnexpectedEof;
                const slice: []u8 = buf[0..len];
                _ = try writer.write(slice);
                pos += len;
            }

            if (pos < size)
                return error.BodyTooSmall;
            return pos;
        }

        inline fn decompressor(self: Self, allocator: std.mem.Allocator) !std.io.AnyReader {
            var fbs = std.io.fixedBufferStream(self.body);
            switch (self.method) {
                .Deflate => {
                    var decom = std.compress.flate.decompressor(
                        fbs.reader().any(),
                    );
                    return decom.reader().any();
                },
                .Gzip => {
                    var decom = std.compress.gzip.decompressor(
                        fbs.reader().any(),
                    );
                    return decom.reader().any();
                },
                .XZ => {
                    var decom = try std.compress.xz.decompress(
                        allocator,
                        fbs.reader().any(),
                    );
                    return decom.reader().any();
                },
                .ZStd => {
                    const default_window_buffer_len = std.compress.zstd.DecompressorOptions.default_window_buffer_len;
                    var window_buffer: [default_window_buffer_len]u8 = undefined;
                    var decom = std.compress.zstd.decompressor(
                        fbs.reader().any(),
                        .{
                            .window_buffer = &window_buffer,
                        },
                    );
                    return decom.reader().any();
                },
                .Raw => {
                    return fbs.reader().any();
                },
                else => return error.NotImplemented,
            }
        }
    };
}
pub fn EntryMap(comptime entry: type) type {
    return struct {
        []const u8,
        entry,
    };
}

/// A Lazy decompressed hashmap of entries
pub fn LazyMap(comptime entry: type, comptime map_type: type) type {
    return struct {
        map: map_type,
        buf: std.hash_map.StringHashMap(entry),

        /// free all entries of 'buf'
        pub fn reset(self: *@This()) void {
            var iterator = self.buf.iterator();
            while (iterator.next()) |it| {
                if (self.map.get(it.key_ptr.*).?.method != .Raw) {
                    self.buf.allocator.free(it.value_ptr.body);
                }
                self.buf.allocator.destroy(it.value_ptr);
                self.buf.allocator.free(it.key_ptr.*);
            }
            self.buf.clearAndFree();
        }

        /// free and entry from 'buf' by key
        pub fn free(self: *@This(), key: []const u8) void {
            if (self.buf.get(key)) |v| {
                if (self.map.get(key).?.method != .Raw) {
                    self.buf.allocator.free(v.body);
                }
                _ = self.buf.remove(key);
            }
        }

        pub fn deinit(self: *@This()) void {
            self.reset();
            self.buf.deinit();
        }

        /// gets an entry from the map by caching decompressed entries in 'buf'.
        /// entry.method is guaranteed to be 'Raw'.
        /// when called multiple times, entries of 'buf' are returned directly.
        pub fn get(self: *@This(), key: []const u8) !?*const entry {
            if (self.map.get(key) == null) return null;
            const new_e = try self.buf.getOrPut(key);
            if (new_e.found_existing) {
                return new_e.value_ptr;
            }
            if (self.map.get(key)) |e| {
                new_e.value_ptr.* = e;
                if (e.method == .Raw) return new_e.value_ptr;

                new_e.value_ptr.method = .Raw;
                new_e.value_ptr.body = try e.decompressBodyAlloc(self.buf.allocator);
                return new_e.value_ptr;
            }
            return error.HashMapMiss;
        }
    };
}
