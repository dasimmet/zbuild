const std = @import("std");

pub const CoverageRun = struct {
    const Self = @This();
    run: *std.Build.Step.Run,
    step: *std.Build.Step,
    coverage_dir: ?std.Build.LazyPath = null,

    pub const Options = struct {
        name: []const u8 = "coveragerun",
        exe: *std.Build.Step.Compile,
        coverage: bool = false,
        coveralls_id: ?[]const u8 = null,
    };

    pub fn add(opt: Options) Self {
        var this: Self = undefined;
        const exe = opt.exe;
        const b = exe.step.owner;

        if (opt.coverage) {
            this.run = b.addSystemCommand(&[_][]const u8{
                "kcov",
                "--exclude-path",
                b.graph.zig_lib_directory.path.?,
            });
            this.run.step.name = opt.name;
            if (opt.coveralls_id) |cov_id| {
                this.run.addArg("--coveralls-id");
                this.run.addArg(cov_id);
            }
            this.coverage_dir = this.run.addOutputDirectoryArg(".");
            this.run.addArtifactArg(exe);

            const install = b.addInstallDirectory(.{
                .source_dir = this.coverage_dir.?,
                .install_dir = .{ .custom = "coverage" },
                .install_subdir = exe.name,
            });
            install.step.dependOn(&this.run.step);
            this.step = &install.step;
        } else {
            this.run = b.addRunArtifact(exe);
            this.step = &this.run.step;
        }
        return this;
    }
};
