//! A Library extending the zig build system with some useful steps
//!
//! [Coverage](coverage)
//! [Source code on Gitlab](https://gitlab.com/dasimmet/zbuild)

pub const ZBuild = @This();
const std = @import("std");
const builtin = @import("builtin");

/// Version of zig used to build autodocs
pub const zig_version = builtin.zig_version_string;

///! some shared useful libs
pub const CompressHeader = @import("build/CompressHeader.zig");
pub const CompressMap = @import("build/CompressMap.zig");
pub const DownloadModule = @import("build/DownloadModule.zig");
pub const fmt = @import("fmt.zig");
pub const JsonToZon = @import("build/JsonToZon.zig");
pub const LazyType = @import("build/LazyType.zig");
pub const Proc = @import("Proc.zig");
pub const RecursiveDirIterator = @import("build/RecursiveDirIterator.zig");

/// Custom Build Steps
pub const Step = struct {
    pub const Compress = @import("build/Step/Compress.zig");
    pub const Cargo = @import("build/Step/Cargo.zig");
    pub const Download = @import("build/Step/Download.zig");
    pub const Emsdk = @import("build/Step/Emsdk.zig");
    pub const AndroidSdk = @import("build/Step/AndroidSdk.zig");
    pub const Fetch = @import("build/Step/Fetch.zig");
    pub const JZon = @import("build/Step/JZon.zig");
    pub const Sdk = @import("build/Step/Sdk.zig");
    pub const Serve = @import("build/Step/Serve.zig");
};
pub const Test = @import("build/Test.zig");
pub const Zon = @import("build/Zon.zig");

/// Shared sources for the Server
pub const Srv = struct {
    pub const Process = @import("srv/Process.zig");
};

/// options for the `zig build zb` step
pub const Options = struct {
    name: []const u8 = "zb",
    menu: ?[]const u8 = null,
    owner: *std.Build,
    optimize: ?std.builtin.OptimizeMode = null,
    target: ?std.Build.ResolvedTarget = null,
    offline: bool = false,
};

steps: struct {
    exe: *std.Build.Step.Compile,
    top_level: *std.Build.Step,
    run: *std.Build.Step.Run,
},
options: Options,

/// stores a global instance of ZBuild after init()
pub var ZBuild_global: ZBuild = undefined;
/// Initializes the `zig build zb` step
pub fn init(opt: Options) ZBuild {
    const b = opt.owner;

    // std.log.info("target: {any}", .{target.?.type_id});

    const zb_step = b.step("zb", "zbuild command line");
    const zb_exe = b.addExecutable(.{
        .name = opt.name,
        .root_source_file = .{
            .cwd_relative = b.pathJoin(&[_][]const u8{
                @src().file,
                "..",
                "cli.zig",
            }),
        },
        .target = opt.target orelse b.resolveTargetQuery(std.Target.Query{}),
        .optimize = opt.optimize orelse .Debug,
    });
    zb_exe.root_module.addAnonymousImport("ZBuild", .{
        .root_source_file = .{ .cwd_relative = @src().file },
    });
    const options = b.addOptions();
    options.addOption([]const u8, "cache_root", b.cache_root.path.?);
    options.addOption(
        []const u8,
        "global_cache_root",
        b.graph.global_cache_root.path.?,
    );
    zb_exe.root_module.addOptions("options", options);

    inline for (&.{
        .{
            .url = "https://raw.githubusercontent.com/ziglang/zig/a931bfada5e358ace980b2f8fbc50ce424ced526/src/Package/Manifest.zig",
            .name = "Zig.Manifest",
            .path = std.fs.path.join(b.allocator, &[_][]const u8{
                @src().file,
                "../build/gen/zig/Package/Manifest.zig",
            }) catch null,
        },
    }) |it| {
        const mod = DownloadModule.init(.{
            .owner = b,
            .url = it.url,
            .path = it.path,
            .name = it.name,
        });

        if (mod.copy) |cp| zb_exe.step.dependOn(cp.step());
        zb_exe.root_module.addImport(it.name, mod.module);
    }
    const run = b.addRunArtifact(zb_exe);
    if (b.args) |args| {
        run.addArgs(args);
    }
    zb_step.dependOn(&run.step);

    ZBuild_global = .{
        .steps = .{
            .exe = zb_exe,
            .run = run,
            .top_level = zb_step,
        },
        .options = opt,
    };
    return ZBuild_global;
}

test "decls" {
    std.testing.refAllDecls(@This());
    std.testing.refAllDecls(Options);
    std.testing.refAllDecls(Step);
    std.testing.refAllDeclsRecursive(Srv);
    std.testing.refAllDeclsRecursive(Step.Cargo);
    std.testing.refAllDeclsRecursive(Step.Compress);
    std.testing.refAllDecls(Step.Download);
    std.testing.refAllDecls(Step.Fetch);
    std.testing.refAllDecls(Step.Sdk);
    std.testing.refAllDecls(Step.Emsdk);
    std.testing.refAllDecls(Step.JZon);
}
