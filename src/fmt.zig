const std = @import("std");

/// a factor to estimate the amount of fields in a template of size x
pub const fmt_size_factor = 5;

///takes a format string with named templates:
///"{name} {my} {struct} {keys}"
///and a struct of unknown type,
///matches the struct's members and formats to the writer
pub fn format(
    comptime fmt: []const u8,
    writer: anytype,
    args: anytype,
) !void {
    return formatStruct(fmt, fmt.len / fmt_size_factor, writer, args);
}

///same as format but accepting a max_fields argument
pub fn formatStruct(
    fmt: []const u8,
    comptime max_fields: comptime_int,
    writer: anytype,
    args: anytype,
) !void {
    const info = @typeInfo(@TypeOf(args));

    switch (info) {
        .@"struct" => {},
        else => @compileError("Type Error: Struct expected"),
    }

    const buf_size = @sizeOf([]u8) * 2 * max_fields;
    var fields_buffer: [buf_size]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&fields_buffer);
    const allocator = fba.allocator();

    var field_list = std.ArrayListUnmanaged([]const u8){};
    defer field_list.deinit(allocator);

    inline for (info.@"struct".fields) |f| {
        const tpl = comptime std.fmt.comptimePrint(
            "{{{s}}}",
            .{f.name},
        );
        var ofs: usize = 0;
        while (std.mem.indexOf(u8, fmt[ofs..], tpl)) |pos| {
            const end = ofs + pos + tpl.len;
            try field_list.append(allocator, fmt[ofs + pos .. end]);
            ofs = end;
        }
    }
    {
        var pos: usize = 0;
        while (field_list.items.len > 0) {
            var next_item: ?[]const u8 = null;
            var next_i: ?usize = null;
            for (field_list.items, 0..) |it, i| {
                if (next_item == null or @intFromPtr(it.ptr) < @intFromPtr(next_item.?.ptr)) {
                    next_item = it;
                    next_i = i;
                }
            }
            if (next_i) |it| {
                const offset = @intFromPtr(next_item.?.ptr) - @intFromPtr(fmt.ptr);
                _ = field_list.orderedRemove(it);
                try writer.writeAll(fmt[pos..offset]);
                inline for (info.@"struct".fields) |f| {
                    if (std.mem.eql(u8, f.name, next_item.?[1 .. next_item.?.len - 1])) {
                        const value = @field(args, f.name);
                        switch (@typeInfo(f.type)) {
                            .optional => |opt| {
                                const tpl = switch (@typeInfo(opt.child)) {
                                    .pointer => "{s}",
                                    .float,
                                    .int,
                                    => "{d}",
                                    else => "{any}",
                                };

                                if (value) |v| {
                                    try std.fmt.format(
                                        writer,
                                        tpl,
                                        .{v},
                                    );
                                }
                                break;
                            },
                            .pointer => {
                                try std.fmt.format(
                                    writer,
                                    "{s}",
                                    .{std.mem.sliceAsBytes(value)},
                                );
                                break;
                            },
                            .float,
                            .int,
                            => {
                                try std.fmt.format(
                                    writer,
                                    "{d}",
                                    .{value},
                                );
                                break;
                            },
                            else => {
                                try std.fmt.format(
                                    writer,
                                    "{any}",
                                    .{value},
                                );
                                break;
                            },
                        }
                        try std.fmt.format(writer, "{s}", .{value});
                    }
                }
                pos = offset + next_item.?.len;
            }
        }
        try writer.writeAll(fmt[pos..]);
    }
}

test "parse and format url" {
    const url_str = "https://my:test@ziglang.org/documentation/master/#my_fragment";
    const url = try std.Uri.parse(url_str);
    var buf: [url_str.len * 5]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buf);

    try formatStruct(
        "{scheme}://{user}:{password}@{host}{path}#{fragment}",
        url_str.len,
        fbs.writer(),
        url,
    );
    // std.debug.print("res: {s}", .{fbs.getWritten()});
    // try std.testing.expect(std.mem.eql(u8, fbs.getWritten(), url_str));
}
