//!subprocess utils
//!

const std = @import("std");
const StdIo = std.process.Child.StdIo;

pub const CallArgs = struct {
    argv: []const []const u8,
    allocator: std.mem.Allocator,
    cwd: ?[]const u8 = null,
    env_map: ?*std.process.EnvMap = null,
    stdin_behavior: StdIo = .Ignore,
    stdin_bytes: []const u8 = "",
    stdout_behavior: StdIo = .Inherit,
    stderr_behavior: StdIo = .Inherit,
    expect_exitcode: ?u8 = null,
};

pub fn call(args: CallArgs) !u8 {
    var proc = std.process.Child.init(
        args.argv,
        args.allocator,
    );
    proc.cwd = args.cwd;
    proc.env_map = args.env_map;
    proc.stdin_behavior = args.stdin_behavior;
    proc.stdout_behavior = args.stdout_behavior;
    proc.stderr_behavior = args.stderr_behavior;
    try proc.spawn();
    if (args.stdin_behavior == .Pipe) try proc.stdin.?.writeAll(args.stdin_bytes);
    const term = try proc.wait();
    switch (term) {
        .Exited => |e| {
            if (args.expect_exitcode) |expected| {
                if (expected != e)
                    return error.ChildProcessExitcode;
            }
            return e;
        },
        .Signal => |s| {
            std.log.err("Signal: {d}", .{s});
            return error.Signal;
        },
        else => return error.ChildProcess,
    }
}

pub fn check_output(args: struct {
    allocator: std.mem.Allocator,
    cwd: ?[]const u8 = null,
    argv: []const []const u8,
}) ![]const u8 {
    const stderr = std.io.getStdErr().writer();

    const res = try std.process.Child.run(.{
        .allocator = args.allocator,
        .argv = args.argv,
        .cwd = args.cwd,
    });
    try stderr.writeAll(res.stderr);
    args.allocator.free((res.stderr));

    if (res.term != .Exited) {
        try stderr.writeAll("Stdout:\n");
        try stderr.writeAll(res.stdout);
        try stderr.print("\nprocess.Child terminated: {s}:{any}\n", .{ @tagName(res.term), res.term });
        return error.Terminated;
    }
    if (res.term.Exited != 0) {
        try stderr.writeAll("Stdout:\n");
        try stderr.writeAll(res.stdout);
        try stderr.print("\nCommand exited with Status: {d}\n", .{res.term.Exited});
        return error.ExitCode;
    }
    return res.stdout;
}

pub fn git_url(allocator: std.mem.Allocator, path: ?[]const u8) ![]const u8 {
    const stdout = try check_output(.{
        .allocator = allocator,
        .cwd = path,
        .argv = &.{ "git", "config", "remote.origin.url" },
    });
    return std.mem.trim(u8, stdout, " \r\n\t");
}

pub fn git_rev(allocator: std.mem.Allocator, path: ?[]const u8) ![]const u8 {
    const stdout = try check_output(.{
        .allocator = allocator,
        .cwd = path,
        .argv = &.{ "git", "rev-parse", "HEAD" },
    });
    return std.mem.trim(u8, stdout, " \r\n\t");
}

pub fn ls_remote(allocator: std.mem.Allocator, path: []const u8) ![]const u8 {
    const stdout = try check_output(.{
        .allocator = allocator,
        .argv = &.{ "git", "ls-remote", path },
    });
    return std.mem.trim(u8, stdout, " \r\n\t");
}
