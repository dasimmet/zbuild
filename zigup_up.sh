#!/usr/bin/sh
# 
# installs zigup
# curl -L "https://gitlab.com/dasimmet/zbuild/-/raw/master/zigup_up.sh" | sh
# wget -O - "https://gitlab.com/dasimmet/zbuild/-/raw/master/zigup_up.sh" | sh

set -ex
which mktemp > /dev/null

BASE="https://github.com/marler8997/zigup/releases"
VERSION="latest"
FILENAME="zigup-x86_64-linux.tar.gz"
URL="$BASE/$VERSION/download/$FILENAME"
if which systemd-path > /dev/null; then
    BIN_DIR="$(systemd-path user-binaries)"
else
    BIN_DIR="$HOME/.local/bin"
fi

if ! which curl > /dev/null; then
    if ! which wget > /dev/null; then
        echo "this script needs either 'curl' or 'wget'"
        exit 1
    fi
    FETCH_CMD="wget -O"
else
    FETCH_CMD="curl -L -o"
fi
if ! which tar > /dev/null; then
        echo "this script needs 'tar'"
        exit 1
fi

TMPFILE="`mktemp -u`.$FILENAME"
cleanup(){
    rm -f "$TMPFILE"
}
trap cleanup EXIT

mkdir -p "$BIN_DIR"
$FETCH_CMD "$TMPFILE" -- "$URL"
cd "$BIN_DIR"
tar xf "$TMPFILE" zigup
chmod +x zigup

if [ "$1" != "" ];
then
    ./zigup "$1"
fi