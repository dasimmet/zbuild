const builtin = @import("builtin");
const std = @import("std");
pub const ZBuild = @import("src/ZBuild.zig");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const zb_mod = b.addModule("ZBuild", .{
        .root_source_file = b.path("src/ZBuild.zig"),
    });
    _ = zb_mod;
    const zb = ZBuild.init(.{
        .owner = b,
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(zb.steps.exe);

    const tests = b.addTest(.{
        .name = "ZBuild",
        .root_source_file = b.path("src/ZBuild.zig"),
        .target = target,
        .optimize = .Debug,
    });

    const coverall_opt = b.option(
        []const u8,
        "coveralls-id",
        "token for uploading coverage to coverall.io",
    );
    const coverage_opt = b.option(
        bool,
        "coverage",
        "output coverage by running test with kcov",
    ) orelse (coverall_opt != null);
    const coverage = ZBuild.Test.CoverageRun.add(.{
        .exe = tests,
        .coverage = coverage_opt,
        .coveralls_id = coverall_opt,
    });

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(coverage.step);

    const lib_docs = tests.getEmittedDocs();
    const docs = b.addInstallDirectory(.{
        .source_dir = lib_docs,
        .install_dir = .prefix,
        .install_subdir = "docs",
    });

    const assets = ZBuild.Step.Compress.init(.{
        .build = b,
        .dir = lib_docs,
        .name = "assets",
        .method = b.option(
            ZBuild.Step.Compress.Method,
            "compress",
            "which compression method to use in Step.Compress",
        ) orelse .Deflate,
    });
    assets.addFile(.{
        .map_path = b.path("README.md"),
        .source_path = b.path("README.md"),
        .method = .Raw,
    });

    const decompress = assets.exe(b);
    const decompress_step = b.step("decompress", "decompress an entry from assets");
    var decompress_run = b.addRunArtifact(decompress);
    decompress_run.addArg("index.html");
    decompress_run.addArg("zig-out/index.html");
    decompress_step.dependOn(&decompress_run.step);

    const Server = ZBuild.Step.Serve.init(.{
        .owner = b,
        .port = b.option(
            u16,
            "port",
            "for to use for serve step",
        ) orelse 8000,
        // replace assets with public_folder to serve at runtime
        .assets = assets.module(b),
        // .public_folder = lib_docs,
        .name = "serve",
        .options = .{
            .name = "zbuild-serve",
            .target = target,
            .optimize = optimize,
        },
    });

    const install = b.addInstallArtifact(
        Server.compile,
        .{},
    );

    const run = b.step("run", "run the server");
    run.dependOn(&Server.run(b).step);

    const Emsdk = ZBuild.Step.Emsdk.init(.{
        .owner = b,
        .name = "emsdk",
        .libs = &.{
            "zlib",
            "sqlite3",
            "libGL",
        },
    });
    const emexe = b.addExecutable(.{
        .name = "emexe",
        .root_source_file = b.path("src/emexe-example.zig"),
        .target = b.resolveTargetQuery(.{
            .cpu_arch = .wasm32,
            .os_tag = .wasi,
        }),
        .optimize = std.builtin.OptimizeMode.ReleaseSmall,
    });
    emexe.rdynamic = true;
    ZBuild.Step.Emsdk.link(Emsdk, emexe);
    emexe.linkSystemLibrary("z");
    emexe.linkLibC();
    emexe.step.dependOn(&Emsdk.step);
    b.step("run-emexe", "run emexe").dependOn(&b.addRunArtifact(emexe).step);

    var download = b.step("fetch", "download the assets");
    download.dependOn(&Emsdk.fetch.step);
    var emsdk_step = b.step("emsdk", "setup emsdk");
    emsdk_step.dependOn(&Emsdk.step);
    emsdk_step.dependOn(&b.addInstallArtifact(emexe, .{}).step);

    const Sdk = ZBuild.Step.Sdk.init(.{
        .owner = b,
        .name = "zlib",
        .url = .{
            .resolved = "https://github.com/madler/zlib/archive/ac8f12c97d1afd9bafa9c710f827d40a407d3266.tar.gz",
        },
        .hash = .{ .resolved = "1220411698fbe1acc0c7ab3effd55e03aad0166c68c012afa4bc094f52f20ecfb495" },
        .cacheFn = SetupConfigureMake.cache,
        .setupFn = SetupConfigureMake.setup,
    });
    _ = Sdk.relativePath("libz.a");
    var sdk_step = b.step("sdk", "download and setup generic sdk");
    download.dependOn(&Sdk.fetch.step);
    sdk_step.dependOn(&Sdk.step);

    const AndSdk = ZBuild.Step.AndroidSdk.init(b);
    var android_sdk_step = b.step("android-sdk", "download and setup android sdk");
    android_sdk_step.dependOn(&AndSdk.sdk.step);

    var install_step = b.getInstallStep();
    install_step.dependOn(&install.step);
    install_step.dependOn(&docs.step);

    const indent_option = b.option(
        u8,
        "mime-indent",
        "indentation of mime zig file",
    ) orelse 0;

    const mime_ext_json = ZBuild.Step.Download.init(
        b,
        .{
            .resolved = "https://raw.githubusercontent.com/patrickmccallum/mimetype-io/9682c0fbe505ff86639f273e5efd6e7be99590bc/src/mimeData.json",
        },
        "mimedata",
    );
    const mime_ext_zig = ZBuild.Step.JZon.init(
        b,
        .{
            .generated = .{ .file = &mime_ext_json.output_file },
        },
        "mimedata",
    );
    mime_ext_zig.indent = indent_option;

    const mime_magic_json = ZBuild.Step.Download.init(
        b,
        .{
            // from: https://github.com/dimapaloskin/detect-file-type
            .resolved = "https://raw.githubusercontent.com/dimapaloskin/detect-file-type/master/signatures.json",
        },
        "mimedata",
    );
    const mime_magic_zig = ZBuild.Step.JZon.init(
        b,
        .{ .generated = .{ .file = &mime_magic_json.output_file } },
        "mimedata",
    );
    mime_magic_zig.indent = indent_option;

    const update_mime = b.addUpdateSourceFiles();
    update_mime.addCopyFileToSource(
        .{ .generated = .{ .file = &mime_ext_zig.output_file } },
        "src/build/gen/mime_extensions.json.zig",
    );
    update_mime.addCopyFileToSource(
        .{ .generated = .{ .file = &mime_magic_zig.output_file } },
        "src/build/gen/mime_magic_numbers.json.zig",
    );
    const mime_step = b.step("mimedata", "Download and convert the mime data");
    mime_step.dependOn(&update_mime.step);

    b.step("fmt", "format source").dependOn(&b.addFmt(.{
        .paths = &[_][]const u8{
            "build.zig",
            "build.zig.zon",
            "src",
        },
        .exclude_paths = &[_][]const u8{
            "src/build/gen",
        },
        .check = b.option(bool, "fmtcheck", "check mode of zig fmt") orelse false,
    }).step);
}

const SetupConfigureMake = struct {
    pub fn cache(ctx: *const ZBuild.Step.Sdk.SetupContext) !void {
        _ = try ctx.man.addFile(@src().file, null);
        ctx.prog.increaseEstimatedTotalItems(3);
    }

    pub fn setup(ctx: *const ZBuild.Step.Sdk.SetupContext) !void {
        const path = ctx.sdk.output_dir.path.?;
        std.log.debug("SetupSDK Path: {s}", .{path});
        const b = ctx.sdk.step.owner;
        const allocator = b.allocator;
        const configure_path = try std.fs.path.join(allocator, &[_][]const u8{ path, "configure" });
        defer allocator.free(configure_path);

        {
            var fd = try std.fs.cwd().openFile(configure_path, .{});
            defer fd.close();

            try fd.chmod(0o777);
        }
        ctx.prog.completeOne();

        var env = try ZBuild.Step.Sdk.zigCCEnv(b);
        defer env.deinit();

        const configure_cmd = &[_][]const u8{
            configure_path,
            "--static",
        };
        _ = try ZBuild.Proc.call(.{
            .allocator = allocator,
            .argv = configure_cmd,
            .cwd = path,
            .env_map = &env,
            .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
            .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
            .expect_exitcode = 0,
        });
        ctx.prog.completeOne();

        var cpu_count_buf: [10]u8 = undefined;
        const cpu_count = try std.fmt.bufPrint(
            &cpu_count_buf,
            "{d}",
            .{std.Thread.getCpuCount() catch 1},
        );
        _ = try ZBuild.Proc.call(.{
            .allocator = allocator,
            .argv = &[_][]const u8{
                "make",
                "-j",
                cpu_count,
            },
            .cwd = path,
            .env_map = &env,
            .stdout_behavior = if (b.verbose) .Inherit else .Ignore,
            .stderr_behavior = if (b.verbose) .Inherit else .Ignore,
            .expect_exitcode = 0,
        });
        ctx.prog.completeOne();
    }
};
